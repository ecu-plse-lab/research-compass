/**
 * Created by Sidharth on 8/7/15.
 */
function handleEdit() {
    $("#appbundle_stepsection_submit").prop('disabled', false);

    $(window).bind('beforeunload', function() {
        return "You have made changes to this page. Do you wish to submit before leaving?";
    });
}

function cancelForm(cancelTo) {
    window.location = cancelTo;
}

$(document).ready(function() {
    $("#appbundle_stepsection_name").keydown(function() {
        handleEdit();
    });
    $("#appbundle_stepsection_ordering").keydown(function() {
        handleEdit();
    });

    $("#appbundle_stepsection_submit").click(function() {
        $(window).unbind('beforeunload');
        $("#appbundle_stepsection_submit").submit();
    });
});