/**
 * Created by Sidharth on 8/12/15.
 */
function handleEdit() {
    $("#appbundle_user_profile_submit").prop('disabled', false);

    $(window).bind('beforeunload', function() {
        return "You have made changes to this page. Do you wish to submit before leaving?";
    });
}

function cancelForm(cancelTo) {
    window.location = cancelTo;
}

$(document).ready(function() {
    $("#appbundle_user_profile_firstName").keydown(function() {
        handleEdit();
    });
    $("#appbundle_user_profile_lastName").keydown(function() {
        handleEdit();
    });
    $("#appbundle_user_profile_displayName").keydown(function() {
        handleEdit();
    });
    $("#appbundle_user_profile_department").keydown(function() {
        handleEdit();
    });
    $("#appbundle_user_profile_phoneNumber").keydown(function() {
        handleEdit();
    });
    $("#appbundle_user_profile_campusLocation").keydown(function() {
        handleEdit();
    });
    $("#appbundle_user_profile_email").keydown(function() {
        handleEdit();
    });

    $("#appbundle_user_profile_submit").click(function() {
        $(window).unbind('beforeunload');
        $("#appbundle_user_profile_submit").submit();
    });
});