/**
 * Created by Sidharth on 8/12/15.
 */
function handleEdit() {
    $("#appbundle_link_submit").prop('disabled', false);

    $(window).bind('beforeunload', function() {
        return "You have made changes to this page. Do you wish to submit before leaving?";
    });
}

function cancelForm(cancelTo) {
    window.location = cancelTo;
}

$(document).ready(function() {
    $("#appbundle_link_name").keydown(function() {
        handleEdit();
    });
    $("#appbundle_link_description").keydown(function() {
        handleEdit();
    });
    $("#appbundle_link_link").keydown(function() {
        handleEdit();
    });

    $("#appbundle_link_submit").click(function() {
        $(window).unbind('beforeunload');
        $("#appbundle_link_submit").submit();
    });
});