function handleEdit() {
    $("#appbundle_project_submit").prop('disabled', false);

    $(window).bind('beforeunload', function() {
        return "You have made changes to this page. Do you wish to save before leaving?";
    });    
}

function cancelForm() {
    window.location = '/project';
}

$(document).ready(function() {
    $("#appbundle_project_projectName").keydown(function() {
        handleEdit();
    });
    $("#appbundle_project_description").keydown(function() {
        handleEdit();
    });
    $("#appbundle_project_department").change(function() {
        handleEdit();
    });

    $("#appbundle_project_submit").click(function() {
        $(window).unbind('beforeunload');
        $("#appbundle_project_submit").submit();
    });
});

