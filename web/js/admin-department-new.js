/**
 * Created by Sidharth on 8/12/15.
 */
function handleEdit() {
    $("#appbundle_department_submit").prop('disabled', false);

    $(window).bind('beforeunload', function() {
        return "You have made changes to this page. Do you wish to submit before leaving?";
    });
}

function cancelForm(cancelTo) {
    window.location = cancelTo;
}

$(document).ready(function() {
    $("#appbundle_department_name").keydown(function() {
        handleEdit();
    });
    $("#appbundle_department_college").keydown(function() {
        handleEdit();
    });

    $("#appbundle_department_submit").click(function() {
        $(window).unbind('beforeunload');
        $("#appbundle_department_submit").submit();
    });
});