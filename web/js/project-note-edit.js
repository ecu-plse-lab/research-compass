function handleEdit() {
    $("#appbundle_projectnote_submit").prop('disabled', false);

    $(window).bind('beforeunload', function() {
        return "You have made changes to this page. Do you wish to save before leaving?";
    });    
}

function cancelForm(projectId, stepId, sectionId) {
    window.location = '/project/section/' + projectId + '/' + stepId + '/' + sectionId;
}

$(document).ready(function() {
    $("#appbundle_projectnote_contents").keydown(function() {
        handleEdit();
    });

    $("#appbundle_projectnote_submit").click(function() {
        $(window).unbind('beforeunload');
        $("#appbundle_projectnote_submit").submit();
    });
});

