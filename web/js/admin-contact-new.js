/**
 * Created by Sidharth on 8/12/15.
 */
function handleEdit() {
    $("#appbundle_contact_submit").prop('disabled', false);

    $(window).bind('beforeunload', function() {
        return "You have made changes to this page. Do you wish to submit before leaving?";
    });
}

function cancelForm(cancelTo) {
    window.location = cancelTo;
}

$(document).ready(function() {
    $("#appbundle_contact_lastName").keydown(function() {
        handleEdit();
    });
    $("#appbundle_contact_firstName").keydown(function() {
        handleEdit();
    });
    $("#appbundle_contact_middleName").keydown(function() {
        handleEdit();
    });
    $("#appbundle_contact_displayName").keydown(function() {
        handleEdit();
    });
    $("#appbundle_contact_contactEmail").keydown(function() {
        handleEdit();
    });
    $("#appbundle_contact_contactPhone").keydown(function() {
        handleEdit();
    });
    $("#appbundle_contact_officeLocation").keydown(function() {
        handleEdit();
    });

    $("#appbundle_contact_submit").click(function() {
        $(window).unbind('beforeunload');
        $("#appbundle_contact_submit").submit();
    });
});