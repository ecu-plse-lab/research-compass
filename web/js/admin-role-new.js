/**
 * Created by Sidharth on 8/12/15.
 */
function handleEdit() {
    $("#appbundle_role_submit").prop('disabled', false);

    $(window).bind('beforeunload', function() {
        return "You have made changes to this page. Do you wish to submit before leaving?";
    });
}

function cancelForm(cancelTo) {
    window.location = cancelTo;
}

$(document).ready(function() {
    $("#appbundle_role_roleName").keydown(function() {
        handleEdit();
    });

    $("#appbundle_role_submit").click(function() {
        $(window).unbind('beforeunload');
        $("#appbundle_role_submit").submit();
    });
});