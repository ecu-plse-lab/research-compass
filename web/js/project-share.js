function handleEdit() {
    $("#form_submit").prop('disabled', false);

    $(window).bind('beforeunload', function() {
        return "You have made changes to this page. Do you wish to submit before leaving?";
    });    
}

function cancelForm() {
    window.location = '/project';
}

$(document).ready(function() {
    $("#form_text").keydown(function() {
        handleEdit();
    });

    $("#form_submit").click(function() {
        $(window).unbind('beforeunload');
        $("#form_submit").submit();
    });
});

