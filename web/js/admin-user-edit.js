/**
 * Created by Sidharth on 8/7/15.
 */
function handleEdit() {
    $("#appbundle_user_submit").prop('disabled', false);

    $(window).bind('beforeunload', function() {
        return "You have made changes to this page. Do you wish to submit before leaving?";
    });
}

function cancelForm(cancelTo) {
    window.location = cancelTo;
}

$(document).ready(function() {
    $("#appbundle_user_firstName").keydown(function() {
        handleEdit();
    });
    $("#appbundle_user_lastName").keydown(function() {
        handleEdit();
    });
    $("#appbundle_user_username").keydown(function() {
        handleEdit();
    });
    $("#appbundle_user_displayName").keydown(function() {
        handleEdit();
    });
    $("appbundle_user_department").keydown(function() {
        handleEdit();
    });
    $("#appbundle_user_email").keydown(function() {
        handleEdit();
    });
    $("#appbundle_user_phoneNumber").keydown(function() {
        handleEdit();
    });
    $("#appbundle_user_campusLocation").keydown(function() {
        handleEdit();
    });

    $("#appbundle_user_submit").click(function() {
        $(window).unbind('beforeunload');
        $("#appbundle_user_submit").submit();
    });
});