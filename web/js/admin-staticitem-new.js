/**
 * Created by Sidharth on 8/12/15.
 */
function handleEdit() {
    $("#appbundle_staticitem_submit").prop('disabled', false);

    $(window).bind('beforeunload', function() {
        return "You have made changes to this page. Do you wish to submit before leaving?";
    });
}

function cancelForm(cancelTo) {
    window.location = cancelTo;
}

$(document).ready(function() {
    $("#appbundle_staticitem_name").keydown(function() {
        handleEdit();
    });
    $("#appbundle_staticitem_step").keydown(function() {
        handleEdit();
    });
    $("#appbundle_staticitem_stepSection").keydown(function() {
        handleEdit();
    });
    $("#appbundle_staticitem_contents").keydown(function() {
        handleEdit();
    });

    $("#appbundle_staticitem_submit").click(function() {
        $(window).unbind('beforeunload');
        $("#appbundle_staticitem_submit").submit();
    });
});