function markComplete(idToSet) {
    var idName = "#appbundle_project_flags_itemStatus_" + idToSet;
    $(idName).val("1");

    var buttonIdName = "#btn-" + idToSet;
    $(buttonIdName).removeClass("btn-default").addClass("btn-success");
    $(buttonIdName).attr('onclick', 'markIncomplete('+idToSet+');');
    $(buttonIdName).attr('alt', 'Mark Incomplete');
    $(buttonIdName).attr('title', 'Mark this item as being incomplete.');

    $("#appbundle_project_flags_submit").prop('disabled', false);

    $(window).bind('beforeunload', function() {
        return "You have made changes to this page. Do you wish to save before leaving?";
    });
}

function markIncomplete(idToSet) {
    var idName = "#appbundle_project_flags_itemStatus_" + idToSet;
    $(idName).val("0");

    var buttonIdName = "#btn-" + idToSet;
    $(buttonIdName).removeClass("btn-success").addClass("btn-default");
    $(buttonIdName).attr('onclick', 'markComplete('+idToSet+');');
    $(buttonIdName).attr('alt', 'Mark Complete');
    $(buttonIdName).attr('title', 'Mark this item as being complete.');

    $("#appbundle_project_flags_submit").prop('disabled', false);

    $(window).bind('beforeunload', function() {
        return "You have made changes to this page. Do you wish to save before leaving?";
    });
}

$(document).ready(function() {
    $("#appbundle_project_flags_submit").click(function() {
        $(window).unbind('beforeunload');
        $("#appbundle_project_flags_submit").submit();
    });
});

