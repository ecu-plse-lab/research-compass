<?php

namespace AppBundle\Twig;

use AppBundle\Entity\LayoutItem;
use AppBundle\Entity\StaticLayoutItem;
use AppBundle\Entity\DocumentLayoutItem;
use AppBundle\Entity\ContactLayoutItem;
use AppBundle\Entity\LinkLayoutItem;
use AppBundle\Entity\RoleLayoutItem;
use AppBundle\Entity\LibraryResourceLayoutItem;

class AppExtension extends \Twig_Extension
{
	public function getTests()
	{
		return array(
			new \Twig_SimpleTest('contact', array($this, 'isContact')),
			new \Twig_SimpleTest('document', array($this, 'isDocument')),
			new \Twig_SimpleTest('link', array($this, 'isLink')),
			new \Twig_SimpleTest('role', array($this, 'isRole')),
			new \Twig_SimpleTest('staticMarkdown', array($this, 'isStaticMarkdown')),
			new \Twig_SimpleTest('libraryResource', array($this, 'isLibraryResource')),
		);
	}

	public function isContact(LayoutItem $item)
	{
		return $item instanceof ContactLayoutItem;
	}

	public function isDocument(LayoutItem $item)
	{
		return $item instanceof DocumentLayoutItem;
	}

	public function isLink(LayoutItem $item)
	{
		return $item instanceof LinkLayoutItem;
	}

	public function isRole(LayoutItem $item)
	{
		return $item instanceof RoleLayoutItem;
	}

	public function isStaticMarkdown(LayoutItem $item)
	{
		return $item instanceof StaticLayoutItem;
	}

	public function isLibraryResource(LayoutItem $item)
	{
		return $item instanceof LibraryResourceLayoutItem;
	}

	public function getName()
	{
		return 'app_extension';
	}
}