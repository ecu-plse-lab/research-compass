<?php
namespace AppBundle\Utils;

class SharingStatus 
{
	const SHARED = 1;
	const ACCEPTED = 2;
	const REJECTED = 3;
}