<?php
namespace AppBundle\Utils;

class Status 
{
	const ACTIVE = 1;
	const INACTIVE = 2;
}