<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Home controller.
 *
 */
class HomeController extends Controller
{

    /**
     * Default homepage for the site
     *
     * @Route("/", name="home")
     * @Method("GET")
     * @Template("AppBundle:Home:home.html.twig")
     */
    public function indexAction()
    {
        if ($this->container->get( 'kernel' )->getEnvironment() == 'prod') {
            $loginLink = 'https://researchcompass.ecu.edu/Shibboleth.sso/Login?entityID=https%3A%2F%2Fsso.ecu.edu%2Fidp%2Fshibboleth&target=https%3A%2F%2Fresearchcompass.ecu.edu%2Fproject';
        } else {
            $loginLink = $this->generateUrl('login');    
        }
    
        return array('loginLink' => $loginLink);
    }

    /**
     * Default admin home
     *
     * @Route("/admin/", name="admin_home")
     * @Method("GET")
     * @Template("AppBundle:Home:admin.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function adminAction()
    {
        return array();
    }

    /**
     * Shibboleth logout
     *
     * @Route("/shiblogout/", name="shib_logout")
     * @Method("GET")
     * @Template("AppBundle:Home:home.html.twig")
     * @Security("has_role('ROLE_USER')")
     */
    public function shiblogoutAction()
    {
        $this->get('session')->invalidate(1);
        return $this->redirect("https://researchcompass.ecu.edu/Shibboleth.sso/Logout?return=https://researchcompass.ecu.edu/");
    }

}