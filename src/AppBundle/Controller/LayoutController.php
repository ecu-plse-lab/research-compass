<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Layout;
use AppBundle\Entity\StaticItem;
use AppBundle\Entity\StaticLayoutItem;
use AppBundle\Entity\Document;
use AppBundle\Entity\DocumentLayoutItem;
use AppBundle\Entity\Contact;
use AppBundle\Entity\ContactLayoutItem;
use AppBundle\Entity\Link;
use AppBundle\Entity\LinkLayoutItem;
use AppBundle\Entity\Role;
use AppBundle\Entity\RoleLayoutItem;
use AppBundle\Entity\LibraryResourceLayoutItem;
use AppBundle\Form\LayoutType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Layout controller.
 *
 * @Route("/admin/layout")
 */
class LayoutController extends Controller
{

    /**
     * Lists all Layout entities.
     *
     * @Route("/", name="layout")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Layout')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Layout entity.
     *
     * @Route("/", name="layout_create")
     * @Method("POST")
     * @Template("AppBundle:Layout:new.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createAction(Request $request)
    {
        $entity = new Layout();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('layout_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Layout entity.
     *
     * @param Layout $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Layout $entity)
    {
        $form = $this->createForm(new LayoutType(), $entity, array(
            'action' => $this->generateUrl('layout_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Layout entity.
     *
     * @Route("/new/{stepId}/{sectionId}", name="layout_new")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction()
    {
        $entity = new Layout();
        $form   = $this->createCreateForm($entity);

        // TODO: Pick up here!
        $em = $this->getDoctrine()->getManager();
        $role = $em->getRepository('AppBundle:Step')->find($roleId);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Layout entity.
     *
     * @Route("/{id}", name="layout_show")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Layout')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Layout entity.');
        }

        $itemsQuery = $em->createQuery(
            'SELECT li
             FROM AppBundle:LayoutItem li
             WHERE li.layout = :layout
             ORDER BY li.order')
        ->setParameter('layout', $entity);
        $items = $itemsQuery->getResult();

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'items'       => $items,
        );
    }

    /**
     * Displays a form to edit an existing Layout entity.
     *
     * @Route("/{id}/edit", name="layout_edit")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Layout')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Layout entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Layout entity.
    *
    * @param Layout $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Layout $entity)
    {
        $form = $this->createForm(new LayoutType(), $entity, array(
            'action' => $this->generateUrl('layout_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Layout entity.
     *
     * @Route("/{id}", name="layout_update")
     * @Method("PUT")
     * @Template("AppBundle:Layout:edit.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Layout')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Layout entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entity->setModified( new \DateTime() );
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('layout_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Layout entity.
     *
     * @Route("/{id}", name="layout_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Layout')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Layout entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('layout'));
    }

    /**
     * Creates a form to delete a Layout entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('layout_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Adds a new item at the given order position
     *
     * @Route("/addAt/{id}/{order}", name="layout_add_at")
     * @Method("GET")
     * @Template("AppBundle:Layout:addAt.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addAt(Request $request, $id, $order)
    {
        $em = $this->getDoctrine()->getManager();
        $layout = $em->getRepository('AppBundle:Layout')->find($id);
        return array(
            'layout' => $layout,
            'order' => $order,
        );
    }

    /**
     * Adds a new static layout item to the layout
     *
     * @Route("/addStaticLayoutItem/{id}/{order}", name="layout_add_static_layout_item")
     * @Method("GET")
     * @Template("AppBundle:Layout:addStatic.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addStaticLayoutItemAction(Request $request, $id, $order)
    {
        $em = $this->getDoctrine()->getManager();
        $layout = $em->getRepository('AppBundle:Layout')->find($id);

        $staticItemsQuery = $em->createQuery(
            'SELECT i 
             FROM AppBundle:StaticItem i
             WHERE i.step = :step AND i.stepSection = :section')
        ->setParameter('step', $layout->getStep()->getId())
        ->setParameter('section', $layout->getStepSection()->getId());

        $staticItems = $staticItemsQuery->getResult();

        return array(
            'layout' => $layout,
            'order' => $order,
            'staticItems' => $staticItems
        );
    }

    /**
     * Inserts a new static layout item into the layout at the proper location
     *
     * @Route("/insertStaticLayoutItem/{id}/{order}/{itemId}", name="layout_insert_static_layout_item")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function insertStaticLayoutItemAction(Request $request, $id, $order, $itemId)
    {
        $em = $this->getDoctrine()->getManager();
        $layout = $em->getRepository('AppBundle:Layout')->find($id);
        $staticItem = $em->getRepository('AppBundle:StaticItem')->find($itemId);

        $itemsToPersist = array();

        foreach ($layout->getLayoutItems() as $layoutItem) {
            if ($layoutItem->getOrder() >= $order) {
                $layoutItem->setOrder($layoutItem->getOrder() + 1);
                $itemsToPersist[] = $layoutItem;
            }
        }
        $staticLayoutItem = new StaticLayoutItem();
        $staticLayoutItem->setLayout($layout);
        $staticLayoutItem->setOrder($order);
        $staticLayoutItem->setStaticItem($staticItem);

        $layout->addLayoutItem($staticLayoutItem);

        $itemsToPersist[] = $staticLayoutItem;
        $itemsToPersist[] = $layout;

        foreach ($itemsToPersist as $item) {
            $em->persist($item);
        }

        $em->flush();

        return $this->redirect($this->generateUrl('layout_show', array('id' => $id)));
    }

    /**
     * Adds a new contact layout item to the layout
     *
     * @Route("/addContactLayoutItem/{id}/{order}", name="layout_add_contact_layout_item")
     * @Method("GET")
     * @Template("AppBundle:Layout:addContact.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addContactLayoutItemAction(Request $request, $id, $order)
    {
        $em = $this->getDoctrine()->getManager();
        $layout = $em->getRepository('AppBundle:Layout')->find($id);
        $contacts = $em->getRepository('AppBundle:Contact')->findAll();

        return array(
            'layout' => $layout,
            'order' => $order,
            'contacts' => $contacts
        );
    }

    /**
     * Inserts a new contact layout item into the layout at the proper location
     *
     * @Route("/insertContactLayoutItem/{id}/{order}/{contactId}", name="layout_insert_contact_layout_item")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function insertContactLayoutItemAction(Request $request, $id, $order, $contactId)
    {
        $em = $this->getDoctrine()->getManager();
        $layout = $em->getRepository('AppBundle:Layout')->find($id);
        $contact = $em->getRepository('AppBundle:Contact')->find($contactId);

        $itemsToPersist = array();

        foreach ($layout->getLayoutItems() as $layoutItem) {
            if ($layoutItem->getOrder() >= $order) {
                $layoutItem->setOrder($layoutItem->getOrder() + 1);
                $itemsToPersist[] = $layoutItem;
            }
        }
        $contactLayoutItem = new ContactLayoutItem();
        $contactLayoutItem->setLayout($layout);
        $contactLayoutItem->setOrder($order);
        $contactLayoutItem->setContact($contact);

        $layout->addLayoutItem($contactLayoutItem);

        $itemsToPersist[] = $contactLayoutItem;
        $itemsToPersist[] = $layout;

        foreach ($itemsToPersist as $item) {
            $em->persist($item);
        }

        $em->flush();

        return $this->redirect($this->generateUrl('layout_show', array('id' => $id)));
    }

    /**
     * Adds a new document layout item to the layout
     *
     * @Route("/addDocumentLayoutItem/{id}/{order}", name="layout_add_document_layout_item")
     * @Method("GET")
     * @Template("AppBundle:Layout:addDocument.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addDocumentLayoutItemAction(Request $request, $id, $order)
    {
        $em = $this->getDoctrine()->getManager();
        $layout = $em->getRepository('AppBundle:Layout')->find($id);
        $documents = $em->getRepository('AppBundle:Document')->findAll();

        return array(
            'layout' => $layout,
            'order' => $order,
            'documents' => $documents
        );
    }

    /**
     * Inserts a new document layout item into the layout at the proper location
     *
     * @Route("/insertDocumentLayoutItem/{id}/{order}/{documentId}", name="layout_insert_document_layout_item")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function insertDocumentLayoutItemAction(Request $request, $id, $order, $documentId)
    {
        $em = $this->getDoctrine()->getManager();
        $layout = $em->getRepository('AppBundle:Layout')->find($id);
        $document = $em->getRepository('AppBundle:Document')->find($documentId);

        $itemsToPersist = array();

        foreach ($layout->getLayoutItems() as $layoutItem) {
            if ($layoutItem->getOrder() >= $order) {
                $layoutItem->setOrder($layoutItem->getOrder() + 1);
                $itemsToPersist[] = $layoutItem;
            }
        }
        $documentLayoutItem = new DocumentLayoutItem();
        $documentLayoutItem->setLayout($layout);
        $documentLayoutItem->setOrder($order);
        $documentLayoutItem->setDocument($document);

        $layout->addLayoutItem($documentLayoutItem);

        $itemsToPersist[] = $documentLayoutItem;
        $itemsToPersist[] = $layout;

        foreach ($itemsToPersist as $item) {
            $em->persist($item);
        }

        $em->flush();

        return $this->redirect($this->generateUrl('layout_show', array('id' => $id)));
    }

    /**
     * Adds a new link layout item to the layout
     *
     * @Route("/addLinkLayoutItem/{id}/{order}", name="layout_add_link_layout_item")
     * @Method("GET")
     * @Template("AppBundle:Layout:addLink.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addLinkLayoutItemAction(Request $request, $id, $order)
    {
        $em = $this->getDoctrine()->getManager();
        $layout = $em->getRepository('AppBundle:Layout')->find($id);
        $links = $em->getRepository('AppBundle:Link')->findAll();

        return array(
            'layout' => $layout,
            'order' => $order,
            'links' => $links
        );
    }

    /**
     * Inserts a new link layout item into the layout at the proper location
     *
     * @Route("/insertLinkLayoutItem/{id}/{order}/{linkId}", name="layout_insert_link_layout_item")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function insertLinkLayoutItemAction(Request $request, $id, $order, $linkId)
    {
        $em = $this->getDoctrine()->getManager();
        $layout = $em->getRepository('AppBundle:Layout')->find($id);
        $link = $em->getRepository('AppBundle:Link')->find($linkId);

        $itemsToPersist = array();

        foreach ($layout->getLayoutItems() as $layoutItem) {
            if ($layoutItem->getOrder() >= $order) {
                $layoutItem->setOrder($layoutItem->getOrder() + 1);
                $itemsToPersist[] = $layoutItem;
            }
        }
        $linkLayoutItem = new LinkLayoutItem();
        $linkLayoutItem->setLayout($layout);
        $linkLayoutItem->setOrder($order);
        $linkLayoutItem->setLink($link);

        $layout->addLayoutItem($linkLayoutItem);

        $itemsToPersist[] = $linkLayoutItem;
        $itemsToPersist[] = $layout;

        foreach ($itemsToPersist as $item) {
            $em->persist($item);
        }

        $em->flush();

        return $this->redirect($this->generateUrl('layout_show', array('id' => $id)));
    }

    /**
     * Adds a new role layout item to the layout
     *
     * @Route("/addRoleLayoutItem/{id}/{order}", name="layout_add_role_layout_item")
     * @Method("GET")
     * @Template("AppBundle:Layout:addRole.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addRoleLayoutItemAction(Request $request, $id, $order)
    {
        $em = $this->getDoctrine()->getManager();
        $layout = $em->getRepository('AppBundle:Layout')->find($id);
        $roles = $em->getRepository('AppBundle:Role')->findAll();

        return array(
            'layout' => $layout,
            'order' => $order,
            'roles' => $roles
        );
    }

    /**
     * Inserts a new role layout item into the layout at the proper location
     *
     * @Route("/insertRoleLayoutItem/{id}/{order}/{roleId}", name="layout_insert_role_layout_item")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function insertRoleLayoutItemAction(Request $request, $id, $order, $roleId)
    {
        $em = $this->getDoctrine()->getManager();
        $layout = $em->getRepository('AppBundle:Layout')->find($id);
        $role = $em->getRepository('AppBundle:Role')->find($roleId);

        $itemsToPersist = array();

        foreach ($layout->getLayoutItems() as $layoutItem) {
            if ($layoutItem->getOrder() >= $order) {
                $layoutItem->setOrder($layoutItem->getOrder() + 1);
                $itemsToPersist[] = $layoutItem;
            }
        }
        $roleLayoutItem = new RoleLayoutItem();
        $roleLayoutItem->setLayout($layout);
        $roleLayoutItem->setOrder($order);
        $roleLayoutItem->setRole($role);

        $layout->addLayoutItem($roleLayoutItem);

        $itemsToPersist[] = $roleLayoutItem;
        $itemsToPersist[] = $layout;

        foreach ($itemsToPersist as $item) {
            $em->persist($item);
        }

        $em->flush();

        return $this->redirect($this->generateUrl('layout_show', array('id' => $id)));
    }

    /**
     * Deletes a layout item
     *
     * @Route("/delete/{id}/{itemId}", name="delete_layout_item")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteLayoutItemAction(Request $request, $id, $itemId)
    {
        $em = $this->getDoctrine()->getManager();
        $layout = $em->getRepository('AppBundle:Layout')->find($id);
        $item = $em->getRepository('AppBundle:LayoutItem')->find($itemId);

        // Since we are removing an item, we want to "fill in the gap"
        // left by the item by decrementing the order of all following
        // items.
        $itemsQuery = $em->createQuery(
            'SELECT li
             FROM AppBundle:LayoutItem li
             WHERE li.layout = :layout
             AND li.order > :order')
        ->setParameter('layout', $layout)
        ->setParameter('order', $item->getOrder());

        $layoutItems = $itemsQuery->getResult();

        foreach($layoutItems as $layoutItem) {
            $layoutItem->setOrder($layoutItem->getOrder()-1);
            $em->persist($layoutItem);
        }

        $em->remove($item);
        $em->flush();

        return $this->redirect($this->generateUrl('layout_show', array('id' => $id)));
    }

    /**
     * Moves up a layout item, essentially swapping its order with the
     * order of the item above. If it already has an order number of 1,
     * we don't move it.
     *
     * @Route("/moveup/{id}/{itemId}", name="move_up_layout_item")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function moveUpLayoutItemAction(Request $request, $id, $itemId)
    {
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('AppBundle:LayoutItem')->find($itemId);

        if ($item->getOrder() > 1) {
            $layout = $em->getRepository('AppBundle:Layout')->find($id);

            $itemsQuery = $em->createQuery(
                'SELECT li
                 FROM AppBundle:LayoutItem li
                 WHERE li.layout = :layout
                 AND li.order = :order')
            ->setParameter('layout', $layout)
            ->setParameter('order', $item->getOrder()-1);

            $layoutItem = $itemsQuery->getSingleResult();

            $layoutItem->setOrder($layoutItem->getOrder()+1);
            $item->setOrder($item->getOrder()-1);
            $em->persist($layoutItem);
            $em->persist($item);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('layout_show', array('id' => $id)));
    }

    /**
     * Moves down a layout item, essentially swapping its order with the
     * order of the item below. If it already has the highest order number,
     * we don't move it.
     *
     * @Route("/movedown/{id}/{itemId}", name="move_down_layout_item")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function moveDownLayoutItemAction(Request $request, $id, $itemId)
    {
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('AppBundle:LayoutItem')->find($itemId);
        $layout = $em->getRepository('AppBundle:Layout')->find($id);

        $itemsQuery = $em->createQuery(
            'SELECT li
             FROM AppBundle:LayoutItem li
             WHERE li.layout = :layout
             AND li.order = :order')
        ->setParameter('layout', $layout)
        ->setParameter('order', $item->getOrder()+1);

        $layoutItems = $itemsQuery->getResult();

        if (count($layoutItems) > 0) {
            $layoutItem = $layoutItems[0];
            $layoutItem->setOrder($layoutItem->getOrder()-1);
            $item->setOrder($item->getOrder()+1);
            $em->persist($layoutItem);
            $em->persist($item);
            $em->flush();            
        }

        return $this->redirect($this->generateUrl('layout_show', array('id' => $id)));
    }

    /**
     * Adds a new library databases layout item to the layout
     *
     * @Route("/addLibraryDatabasesLayoutItem/{id}/{order}", name="layout_add_library_databases_item")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addDatabaseLibrariesLayoutItemAction(Request $request, $id, $order)
    {
        $em = $this->getDoctrine()->getManager();
        $layout = $em->getRepository('AppBundle:Layout')->find($id);

        $itemsToPersist = array();

        foreach ($layout->getLayoutItems() as $layoutItem) {
            if ($layoutItem->getOrder() >= $order) {
                $layoutItem->setOrder($layoutItem->getOrder() + 1);
                $itemsToPersist[] = $layoutItem;
            }
        }
        $dbLayoutItem = new LibraryResourceLayoutItem();
        $dbLayoutItem->setLayout($layout);
        $dbLayoutItem->setOrder($order);

        $layout->addLayoutItem($dbLayoutItem);

        $itemsToPersist[] = $dbLayoutItem;
        $itemsToPersist[] = $layout;

        foreach ($itemsToPersist as $item) {
            $em->persist($item);
        }

        $em->flush();

        return $this->redirect($this->generateUrl('layout_show', array('id' => $id)));
    }    
}
