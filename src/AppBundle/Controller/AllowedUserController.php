<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\AllowedUser;
use AppBundle\Form\AllowedUserType;

/**
 * AllowedUser controller.
 *
 * @Route("/alloweduser")
 */
class AllowedUserController extends Controller
{

    /**
     * Lists all AllowedUser entities.
     *
     * @Route("/", name="alloweduser")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:AllowedUser')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new AllowedUser entity.
     *
     * @Route("/", name="alloweduser_create")
     * @Method("POST")
     * @Template("AppBundle:AllowedUser:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new AllowedUser();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('alloweduser_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a AllowedUser entity.
     *
     * @param AllowedUser $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(AllowedUser $entity)
    {
        $form = $this->createForm(new AllowedUserType(), $entity, array(
            'action' => $this->generateUrl('alloweduser_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new AllowedUser entity.
     *
     * @Route("/new", name="alloweduser_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new AllowedUser();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a AllowedUser entity.
     *
     * @Route("/{id}", name="alloweduser_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:AllowedUser')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AllowedUser entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing AllowedUser entity.
     *
     * @Route("/{id}/edit", name="alloweduser_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:AllowedUser')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AllowedUser entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a AllowedUser entity.
    *
    * @param AllowedUser $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(AllowedUser $entity)
    {
        $form = $this->createForm(new AllowedUserType(), $entity, array(
            'action' => $this->generateUrl('alloweduser_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing AllowedUser entity.
     *
     * @Route("/{id}", name="alloweduser_update")
     * @Method("PUT")
     * @Template("AppBundle:AllowedUser:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:AllowedUser')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AllowedUser entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('alloweduser_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a AllowedUser entity.
     *
     * @Route("/{id}", name="alloweduser_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:AllowedUser')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AllowedUser entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('alloweduser'));
    }

    /**
     * Creates a form to delete a AllowedUser entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('alloweduser_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
