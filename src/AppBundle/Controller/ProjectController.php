<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Project;
use AppBundle\Entity\SharedProject;
use AppBundle\Form\ProjectType;
use AppBundle\Form\ProjectFlagsType;
use AppBundle\Utils\Status;
use AppBundle\Utils\SharingStatus;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Utils\TextHolder;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\LibraryResourceLayoutItem;

/**
 * Project controller.
 *
 * @Route("/project")
 */
class ProjectController extends Controller
{
    private function userIsOwner($project, $user) 
    {
        return ($project->getOwner() == $user);
    }

    private function userIsCollaborator($project, $user)
    {
        $em = $this->getDoctrine()->getManager();
        $projectQuery = $em->createQuery(
            'SELECT p, sp 
             FROM AppBundle:Project p
             JOIN p.sharedProjects sp
             WHERE p.id = :id AND sp.user = :user AND sp.accepted = :status')
        ->setParameter('id', $project->getId())
        ->setParameter('user', $user)
        ->setParameter('status', SharingStatus::ACCEPTED);

        $projects = $projectQuery->getResult();
        if (count($projects) == 1) {
            return true;
        }

        return false;
    }

    private function userIsOwnerOrCollaborator($project, $user)
    {
        if ($this->userIsOwner($project, $user)) {
            return true;
        }

        if ($this->userIsCollaborator($project, $user))
        {
            return true;
        }

        return false;
    }

    /**
     * Lists all Project entities.
     *
     * @Route("/", name="project")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back the projects for just this user
        $projectsQuery = $em->createQuery(
            'SELECT p
             FROM AppBundle:Project p
             WHERE p.owner = :user AND p.status = :status
             ORDER BY p.projectName')
        ->setParameter('user', $user)
        ->setParameter('status', Status::ACTIVE);
        $projects = $projectsQuery->getResult();

        // Get back shared projects which have been accepted
        $sharedProjectsQuery = $em->createQuery(
            'SELECT sp, p 
             FROM AppBundle:SharedProject sp
             JOIN sp.project p
             WHERE sp.user = :user AND sp.accepted = :sharingStatus AND p.status = :status') 
        ->setParameter('user', $user)
        ->setParameter('sharingStatus', SharingStatus::ACCEPTED)
        ->setParameter('status', Status::ACTIVE);

        $shared = $sharedProjectsQuery->getResult();

        // Get back shared projects which have been offered but not yet accepted
        $offeredProjectsQuery = $em->createQuery(
            'SELECT sp 
             FROM AppBundle:SharedProject sp
             WHERE (sp.user = :user OR sp.email = :email) AND sp.accepted = :status') 
        ->setParameter('user', $user)
        ->setParameter('email', $user->getEmail())
        ->setParameter('status', SharingStatus::SHARED);

        $offered = $offeredProjectsQuery->getResult();

        $setUser = false;
        foreach ($offered as $offeredProject) {
            if ($offeredProject->getUser() == null) {
                $offeredProject->setUser($user);
                $em->persist($offeredProject);
                $setUser = true;
            }
        }
        if ($setUser) {
            $em->flush();
        }

        return array(
            'entities' => $projects,
            'shared' => $shared,
            'pending' => count($offered),
        );
    }

    /**
     * Creates a new Project entity.
     *
     * @Route("/", name="project_create")
     * @Method("POST")
     * @Template("AppBundle:Project:new.html.twig")
     * @Security("has_role('ROLE_USER')")
     */
    public function createAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        $project = new Project();

        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());
        $step = $em->getRepository('AppBundle:Step')->findOneByOrdering(1);
        // $section = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(1);
        $project->setOwner($user);
        $project->setStep($step);
        // $project->setSection($section);

        $layoutItems = $em->getRepository('AppBundle:LayoutItem')->findAll();
        $statuses = array();
        foreach($layoutItems as $item) {
            $statuses[$item->getId()] = false;
        }

        $project->setItemStatus($statuses);
        $form = $this->createCreateForm($project);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            $flash = $this->get('braincrafted_bootstrap.flash');
            $flash->success('Project created.');

            return $this->redirect($this->generateUrl('project_home',array('id' => $project->getId())));
        }

        return array(
            'entity' => $project,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Project entity.
     *
     * @param Project $project The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Project $project)
    {
        $form = $this->createForm(new ProjectType(), $project, array(
            'action' => $this->generateUrl('project_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Project entity.
     *
     * @Route("/new", name="project_new")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function newAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());
        $step = $em->getRepository('AppBundle:Step')->findOneByOrdering(1);
        // $section = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(1);

        $project = new Project();
        $project->setOwner($user);
        $project->setDepartment($user->getDepartment());
        $project->setStep($step);
        // $project->setSection($section);

        $form   = $this->createCreateForm($project);

        return array(
            'entity' => $project,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Project entity.
     *
     * @Route("/show/{id}", name="project_show")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function showAction($id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back the given project
        $project = $em->getRepository('AppBundle:Project')->find($id);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        // Make sure the current user is the user related to this entity, we don't
        // want other uses modifying this project
        if (! $this->userIsOwnerOrCollaborator($project, $user)) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Project entity.');   
        }

        // $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $project,
            // 'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Project entity.
     *
     * @Route("/edit/{id}", name="project_edit")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function editAction($id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back the given project
        $project = $em->getRepository('AppBundle:Project')->find($id);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        // Make sure the current user is the user related to this entity, we don't
        // want other uses modifying this project
        if (! $this->userIsOwnerOrCollaborator($project, $user)) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Project entity.');   
        }

        $editForm = $this->createEditForm($project);
        // $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $project,
            'edit_form'   => $editForm->createView(),
            // 'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Project entity.
    *
    * @param Project $project The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Project $project)
    {
        $form = $this->createForm(new ProjectType(), $project, array(
            'action' => $this->generateUrl('project_update', array('id' => $project->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update','disabled' => true));

        return $form;
    }

    /**
     * Edits an existing Project entity.
     *
     * @Route("/{id}", name="project_update")
     * @Method("PUT")
     * @Template("AppBundle:Project:edit.html.twig")
     * @Security("has_role('ROLE_USER')")
     */
    public function updateAction(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back the given project
        $project = $em->getRepository('AppBundle:Project')->find($id);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        // Make sure the current user is the user related to this entity, we don't
        // want other uses modifying this project
        if (! $this->userIsOwnerOrCollaborator($project, $user)) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Project entity.');   
        }

        // $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($project);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('project_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $project,
            'edit_form'   => $editForm->createView(),
            // 'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Project entity.
     *
     * @Route("/delete/{id}", name="project_delete")
     * @Method("GET")
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteAction(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // $form = $this->createDeleteForm($id);
        // $form->handleRequest($request);

        // if ($form->isValid()) {
            $project = $em->getRepository('AppBundle:Project')->find($id);

            if (!$project) {
                throw $this->createNotFoundException('Unable to find Project entity.');
            }

            // Make sure the current user is the user related to this entity, we don't
            // want other uses modifying this project
            if (! $this->userIsOwner($project, $user)) {
                // TODO: Throw a more appropriate error message!
                throw $this->createNotFoundException('Unable to find Project entity.');   
            }

            $project->setStatus(Status::INACTIVE);

            $flash = $this->get('braincrafted_bootstrap.flash');
            $flash->success('Project deleted.');

            $em->persist($project);
            $em->flush();
        // } else {
        //     $flash = $this->get('braincrafted_bootstrap.flash');
        //     $flash->error('Project not deleted: ' . $form->getErrors());
        // }

        return $this->redirect($this->generateUrl('project'));
    }

    // /**
    //  * Creates a form to delete a Project entity by id.
    //  *
    //  * @param mixed $id The entity id
    //  *
    //  * @return \Symfony\Component\Form\Form The form
    //  */
    // private function createDeleteForm($id)
    // {
    //     return $this->createFormBuilder()
    //         ->setAction($this->generateUrl('project_delete', array('id' => $id)))
    //         ->setMethod('GET')
    //         ->add('submit', 'submit', array('label' => 'Delete'))
    //         ->getForm()
    //     ;
    // }

    /**
     * Display the Project home.
     *
     * @Route("/home/{id}", name="project_home")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function homeAction($id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        $project = $em->getRepository('AppBundle:Project')->find($id);

        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        if (! $this->userIsOwnerOrCollaborator($project, $user)) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        $project->setSection(null);
        $em->persist($project);
        $em->flush();

        // $deleteForm = $this->createDeleteForm($id);

        $stepsQuery = $em->createQuery(
            'SELECT s
             FROM AppBundle:Step s
             ORDER BY s.ordering');

        $steps = $stepsQuery->getResult();

        $sectionsQuery = $em->createQuery(
            'SELECT s
             FROM AppBundle:StepSection s
             ORDER BY s.ordering');

        $sections = $sectionsQuery->getResult();
        $sectionsByName = array();
        foreach($sections as $section) {
            $sectionsByName[$section->getName()] = $section;
        }

        return array(
            'entity'      => $project,
            // 'delete_form' => $deleteForm->createView(),
            'steps'       => $steps,
            'sections'    => $sectionsByName,
        );

    }

    /**
     * Displays a form to share an existing Project entity.
     *
     * @Route("/share/{id}", name="project_share")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function shareAction($id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back the given project
        $project = $em->getRepository('AppBundle:Project')->find($id);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        // Make sure the current user is the user related to this entity, we don't
        // want other uses modifying this project
        if (! $this->userIsOwner($project, $user)) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Project entity.');   
        }

        // Create a new form that will let us enter in the IDs to share with
        $textHolder = new TextHolder();
        $form = $this->createShareForm($project, $textHolder);

        return array(
            'project' => $project,
            'entry_form' => $form->createView(),
        );
    }

    /**
    * Creates a form to share a Project entity.
    *
    * @param TextHolder $textHolder A generic holder of text, used for entering emails
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createShareForm(Project $project, TextHolder $textHolder)
    {
        $form = $this->createFormBuilder($textHolder, array(
            'action' => $this->generateUrl('project_update_sharing', array('id' => $project->getId())),
            'method' => 'PUT',))
            ->add('text', 'textarea', array('label' => 'Email Addresses'))
            ->add('submit', 'submit', array('label' => 'Share Project', 'disabled' => true))
            ->getForm();

        return $form;
    }

    /**
     * Edits an existing Project entity.
     *
     * @Route("/share/{id}", name="project_update_sharing")
     * @Method("PUT")
     * @Template("AppBundle:Project:share.html.twig")
     * @Security("has_role('ROLE_USER')")
     */
    public function updateSharingAction(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back the given project
        $project = $em->getRepository('AppBundle:Project')->find($id);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        // Make sure the current user is the user related to this entity, we don't
        // want other uses modifying this project
        if (! $this->userIsOwner($project, $user)) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Project entity.');   
        }

        // $deleteForm = $this->createDeleteForm($id);
        $textHolder = new TextHolder();
        $form = $this->createShareForm($project, $textHolder);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $emails = explode(",", $textHolder->text, 31);
            $flash = $this->get('braincrafted_bootstrap.flash');
            $this->get('session')->getFlashBag()->clear();

            $emailConstraint = new Assert\Email();
            $emailConstraint->message = 'Invalid email address';
            
            $errorsFound = false;
            $validEmails = array();

            foreach ($emails as $email) {
                $email = trim($email);
                $domain = explode("@",$email);
               // $length = count($domain);
                $errors = $this->get('validator')->validate($email, $emailConstraint);
                if (count($errors) > 0) {
                    $form->get('text')->addError(new FormError($email . ' is not a valid email address'));
                    $errorsFound = true;
                } elseif (strcmp(strtolower($email),strtolower($user->getEmail())) == 0) {
                    $form->get('text')->addError(new FormError($email . ' cannot be the same as your email address'));
                    $errorsFound = true;
                } elseif (strcmp(strtolower(substr($domain[1],-8)),".ecu.edu") != 0 and (strcmp(strtolower($domain[1]),"ecu.edu")!=0)) {//allows both @ecu.edu or @xxxx.ecu.edu)
                    $form->get('text')->addError(new FormError($email . ' is not a valid email domain'));
                    $errorsFound = true;
                } else {
                    $validEmails[] = $email;
                }
            }

            if (! $errorsFound) {

                $transport = \Swift_SmtpTransport::newInstance(
                    $this->container->getParameter('mailer_host'), 
                    $this->container->getParameter('mailer_port'), 
                    $this->container->getParameter('mailer_encryption'))
                    ->setUsername($this->container->getParameter('mailer_user'))
                    ->setPassword($this->container->getParameter('mailer_password'))
                    ->setAuthMode($this->container->getParameter('mailer_auth_mode'));
                $mailer = \Swift_Mailer::newInstance($transport);
                $from = $this->container->getParameter('mailer_from_address');
                foreach ($validEmails as $email) {
                    $sp = new SharedProject();
                    $sp->setEmail($email);
                    $sharingUser = $em->getRepository('AppBundle:User')->findOneByEmail($email);
                    if ($sharingUser) {
                        $sp->setUser($sharingUser);
                    }
                    $sp->setProject($project);
                    $em->persist($sp);

                    $message = \Swift_Message::newInstance()
                        ->setSubject('Research Compass: ' . $user->getDisplayName() . ' has shared a project with you')
                        ->setFrom($from)
                        ->setTo($email)
                        ->setBody(
                            $this->renderView(
                                'AppBundle:Project:sharingEmail.html.twig',
                                array('name' => $user->getDisplayName(), 'projectName' => $project->getProjectName())
                            ),
                            'text/html'
                        );

                    $successes = $mailer->send($message);
                    if ($successes > 0) {
                        $flash->success($project->getProjectName() . ' shared with: ' . $email);        
                    } else {
                        $flash->warning($project->getProjectName() . ' shared with: ' . $email . ' but email could not be sent');
                    }
                    
                }
                $em->flush();
                return $this->redirect($this->generateUrl('project'));    
            }
        }

        return array(
            'project' => $project,
            'entry_form' => $form->createView(),
        );
    }

    /**
     * Displays the Project detail page.
     *
     * @Route("/pending", name="project_pending")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function pendingAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back shared projects which have not yet been accepted, this filters
        // on this user so they cannot see the pending projects for other users.
        $sharedProjectsQuery = $em->createQuery(
            'SELECT sp, p, o
             FROM AppBundle:SharedProject sp
             JOIN sp.project p
             JOIN p.owner o
             WHERE sp.user = :user AND sp.accepted = :status') 
        ->setParameter('user', $user)
        ->setParameter('status', SharingStatus::SHARED);

        $shared = $sharedProjectsQuery->getResult();

        return array(
            'entities' => $shared,
        );
    }

    /**
     * Accept a shared project.
     *
     * @Route("/accept/{id}", name="project_accept")
     * @Method("GET")
     * @Template("AppBundle:Project:pending.html.twig")
     * @Security("has_role('ROLE_USER')")
     */
    public function acceptAction(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back this shared project entry
        $entity = $em->getRepository('AppBundle:SharedProject')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Shared Project entity.');
        }

        // Make sure the current user is the user related to this entity, we don't
        // want other uses accepting this shared project
        if ($entity->getUser() != $user) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Shared Project entity.');   
        }

        // If we get here, we have a valid entity and the right user, so accept
        // and save our changes
        $entity->setAccepted(SharingStatus::ACCEPTED);
        $em->persist($entity);
        $em->flush();

        $flash = $this->get('braincrafted_bootstrap.flash');
        $flash->success('Project accepted.');

        return $this->redirect($this->generateUrl('project'));    
    }

    /**
     * Reject a shared project.
     *
     * @Route("/reject/{id}", name="project_reject")
     * @Method("GET")
     * @Template("AppBundle:Project:pending.html.twig")
     * @Security("has_role('ROLE_USER')")
     */
    public function rejectAction(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back this shared project entry
        $entity = $em->getRepository('AppBundle:SharedProject')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Shared Project entity.');
        }

        // Make sure the current user is the user related to this entity, we don't
        // want other uses accepting this shared project
        if ($entity->getUser() != $user) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Shared Project entity.');   
        }

        // If we get here, we have a valid entity and the right user, so accept
        // and save our changes
        $entity->setAccepted(SharingStatus::REJECTED);
        $em->persist($entity);
        $em->flush();

        $flash = $this->get('braincrafted_bootstrap.flash');
        $flash->success('Project declined.');

        return $this->redirect($this->generateUrl('project'));    
    }    

    /**
     * Displays a form representing the current project step.
     *
     * @Route("/step/{id}/{stepid}", name="project_step")
     * @Method("GET")
     * @Template("AppBundle:Project:home.html.twig")
     * @Security("has_role('ROLE_USER')")
     */
    public function stepAction(Request $request, $id, $stepid)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back the given project
        $project = $em->getRepository('AppBundle:Project')->find($id);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        // Make sure the current user is the user related to this entity, we don't
        // want other uses modifying this project
        if (! $this->userIsOwnerOrCollaborator($project, $user)) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Project entity.');   
        }

        // Get back the given step
        $step = $em->getRepository('AppBundle:Step')->find($stepid);
        if (!$step) {
            throw $this->createNotFoundException('Unable to find Step entity.');
        }

        // Change the step to the parameter given
        $project->setStep($step);
        $project->setSection(null);
        $em->persist($project);
        $em->flush();

        // $deleteForm = $this->createDeleteForm($id);

        $stepsQuery = $em->createQuery(
            'SELECT s
             FROM AppBundle:Step s
             ORDER BY s.ordering');

        $steps = $stepsQuery->getResult();

        $sectionsQuery = $em->createQuery(
            'SELECT s
             FROM AppBundle:StepSection s
             ORDER BY s.ordering');

        $sections = $sectionsQuery->getResult();
        $sectionsByName = array();
        foreach($sections as $section) {
            $sectionsByName[$section->getName()] = $section;
        }

        return array(
            'entity'      => $project,
            // 'delete_form' => $deleteForm->createView(),
            'steps'       => $steps,
            'sections'    => $sectionsByName,
        );
    }

    /**
     * Displays a form representing the current project section.
     *
     * @Route("/section/{id}/{stepid}/{sectionid}", name="project_section")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function sectionAction($id, $stepid, $sectionid) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back the given project
        $project = $em->getRepository('AppBundle:Project')->find($id);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        // Make sure the current user is the user related to this entity, we don't
        // want other uses modifying this project
        if (! $this->userIsOwnerOrCollaborator($project, $user)) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Project entity.');   
        }

        // Get back the given step
        $step = $em->getRepository('AppBundle:Step')->find($stepid);
        if (!$step) {
            throw $this->createNotFoundException('Unable to find Step entity.');
        }

        // Get back the given section
        $section = $em->getRepository('AppBundle:StepSection')->find($sectionid);
        if (!$section) {
            throw $this->createNotFoundException('Unable to find Section entity.');
        }

        // Update the step, if it differs
        if ($step != $project->getStep()) {
            $project->setStep($step);
        }

        // Change the section to the parameter given
        $project->setSection($section);
        $em->persist($project);
        $em->flush();

        $stepsQuery = $em->createQuery(
            'SELECT s
             FROM AppBundle:Step s
             ORDER BY s.ordering');

        $steps = $stepsQuery->getResult();

        $layoutQuery = $em->createQuery(
            'SELECT l
             FROM AppBundle:Layout l
             WHERE l.step = :step AND l.stepSection = :section')
        ->setParameter('step', $step)
        ->setParameter('section', $section);
        $layout = $layoutQuery->getSingleResult();

        $itemsQuery = $em->createQuery(
            'SELECT li
             FROM AppBundle:LayoutItem li
             WHERE li.layout = :layout
             ORDER BY li.order')
        ->setParameter('layout', $layout);

        $layoutItems = $itemsQuery->getResult();

        $loadDatabaseList = false;
        $statuses = $project->getItemStatus();
        $dirtyflag = false;
        foreach ($layoutItems as $layoutItem) {
            if (!array_key_exists($layoutItem->getId(), $statuses)) {
                $statuses[$layoutItem->getId()] = false;
                $dirtyflag = true;
            }
            if ($layoutItem instanceof LibraryResourceLayoutItem) {
                $loadDatabaseList = true;
            }
        }
        if ($dirtyflag) {
            $project->setItemStatus($statuses);
            $em->persist($project);
            $em->flush();
        }

        $documentsQuery = $em->createQuery(
            'SELECT d
             FROM AppBundle:ProjectDocument d
             WHERE d.project = :project
             AND d.step = :step
             AND d.section = :section
             ORDER BY d.name')
        ->setParameter('project', $project)
        ->setParameter('step', $step)
        ->setParameter('section', $section);

        $documents = $documentsQuery->getResult();

        $notesQuery = $em->createQuery(
            'SELECT n
             FROM AppBundle:ProjectNote n
             WHERE n.project = :project
             AND n.step = :step
             AND n.section = :section
             ORDER BY n.created')
        ->setParameter('project', $project)
        ->setParameter('step', $step)
        ->setParameter('section', $section);

        $notes = $notesQuery->getResult();

        $databasePriorityList = null;
        $databaseList = null;
        if ($loadDatabaseList) {
            $priorityByCollegeQuery = $em->createQuery(
                'SELECT l
                FROM AppBundle:LibraryResource l
                JOIN l.colleges c
                WHERE c.college = :c AND c.priority = true
                ORDER BY l.title')
            ->setParameter('c', $project->getDepartment()->getCollege());
            $regularByCollegeQuery = $em->createQuery(
                'SELECT l
                FROM AppBundle:LibraryResource l
                JOIN l.colleges c
                WHERE c.college = :c AND c.priority = false
                ORDER BY l.title')
            ->setParameter('c', $project->getDepartment()->getCollege());
            $priorityByDepartmentQuery = $em->createQuery(
                'SELECT l
                FROM AppBundle:LibraryResource l
                JOIN l.departments d
                WHERE d.department = :d AND d.priority = true
                ORDER BY l.title')
            ->setParameter('d', $project->getDepartment());
            $regularByDepartmentQuery = $em->createQuery(
                'SELECT l
                FROM AppBundle:LibraryResource l
                JOIN l.departments d
                WHERE d.department = :d AND d.priority = true
                ORDER BY l.title')
            ->setParameter('d', $project->getDepartment());

            $collegePriority = $priorityByCollegeQuery->getResult();
            $collegeRegular = $regularByCollegeQuery->getResult();
            $departmentPriority = $priorityByDepartmentQuery->getResult();
            $departmentRegular = $regularByDepartmentQuery->getResult();

            $databasePriorityList = array_merge($collegePriority, $departmentPriority);
            $databaseList = array_merge($collegeRegular, $departmentRegular);

            uasort($databasePriorityList, function($a, $b) { return strnatcasecmp($a->getTitle(), $b->getTitle()); });
            uasort($databaseList, function($a, $b) { return strnatcasecmp($a->getTitle(), $b->getTitle()); });
        }

        // Find the next and previous step sections to go to
        // NOTE: This is based on our current knowledge of the system,
        // where we have a fixed number of steps and sections. We should
        // change this to softcode it, in case this changes.
        // TODO: Compute this algorithmically
        if ($step->getOrdering() == 1) {
            $priorStep = null;
            $nextStep = $em->getRepository('AppBundle:Step')->findOneByOrdering(2);
        } elseif ($step->getOrdering() == 12) {
            $priorStep = $em->getRepository('AppBundle:Step')->findOneByOrdering(11);
            $nextStep = null;
        } else {
            $priorStep = $em->getRepository('AppBundle:Step')->findOneByOrdering($step->getOrdering()-1);
            $nextStep = $em->getRepository('AppBundle:Step')->findOneByOrdering($step->getOrdering()+1);
        }

        if ($section->getOrdering() == 1) {
            $nextStep = null; // TODO: Move this computation so we don't hit the DB
            if (isset($priorStep)) {
                $priorSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(6);
                $nextSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(2);
            } else {
                $priorSection = null;
                $nextSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(2);
            }
        } elseif ($section->getOrdering() == 6) {
            $priorStep = null; // TODO: Move this computation so we don't hit the DB
            if (isset($nextStep)) {
                $priorSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(5);
                $nextSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(1);
            } else {
                $priorSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(5);
                $nextSection = null;
            }
        } else {
            $nextStep = null; // TODO: Move this computation so we don't hit the DB
            $priorStep = null; // TODO: Move this computation so we don't hit the DB
            $priorSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering($section->getOrdering()-1);
            $nextSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering($section->getOrdering()+1);
        }

        $form = $this->createForm(new ProjectFlagsType(), $project, array(
            'action' => $this->generateUrl('project_section_update',
                array('id' => $project->getId(), 'stepid' => $step->getId(),
                      'sectionid' => $section->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Save', 'disabled' => 'true'));

        return array(
            'entity'       => $project,
            'steps'        => $steps,
            'layout'       => $layout,
            'layoutItems'  => $layoutItems,
            'form'         => $form->createView(),
            'documents'    => $documents,
            'notes'        => $notes,
            'priorStep'    => $priorStep,
            'nextStep'     => $nextStep,
            'priorSection' => $priorSection,
            'nextSection'  => $nextSection,
            'dbPriority'   => $databasePriorityList,
            'dbList'       => $databaseList,
        );
    }

    /**
     * Edits an existing Project entity.
     *
     * @Route("/section-update/{id}/{stepid}/{sectionid}", name="project_section_update")
     * @Method("POST")
     * @Template("AppBundle:Project:section.html.twig")
     * @Security("has_role('ROLE_USER')")
     */
    public function sectionUpdateAction(Request $request, $id, $stepid, $sectionid)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back the given project
        $project = $em->getRepository('AppBundle:Project')->find($id);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        // Make sure the current user is the user related to this entity, we don't
        // want other uses modifying this project
        if (! $this->userIsOwnerOrCollaborator($project, $user)) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Project entity.');   
        }

        // Get back the given step
        $step = $em->getRepository('AppBundle:Step')->find($stepid);
        if (!$step) {
            throw $this->createNotFoundException('Unable to find Step entity.');
        }

        // Get back the given section
        $section = $em->getRepository('AppBundle:StepSection')->find($sectionid);
        if (!$section) {
            throw $this->createNotFoundException('Unable to find Section entity.');
        }

        $form = $this->createForm(new ProjectFlagsType(), $project, array(
            'action' => $this->generateUrl('project_section_update',
                array('id' => $project->getId(), 'stepid' => $step->getId(),
                      'sectionid' => $section->getId())),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Update'));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('project_section', 
                array('id' => $id, 'stepid' => $stepid, 'sectionid' => $sectionid)));
        }

        $stepsQuery = $em->createQuery(
            'SELECT s
             FROM AppBundle:Step s
             ORDER BY s.ordering');

        $steps = $stepsQuery->getResult();

        $layoutQuery = $em->createQuery(
            'SELECT l
             FROM AppBundle:Layout l
             WHERE l.step = :step AND l.stepSection = :section')
        ->setParameter('step', $step)
        ->setParameter('section', $section);
        $layout = $layoutQuery->getSingleResult();

        $itemsQuery = $em->createQuery(
            'SELECT li
             FROM AppBundle:LayoutItem li
             WHERE li.layout = :layout
             ORDER BY li.order')
        ->setParameter('layout', $layout);

        $layoutItems = $itemsQuery->getResult();

        $loadDatabaseList = false;
        foreach ($layoutItems as $layoutItem) {
            if ($layoutItem instanceof LibraryResourceLayoutItem) {
                $loadDatabaseList = true;
            }
        }

        $documentsQuery = $em->createQuery(
            'SELECT d
             FROM AppBundle:ProjectDocument d
             WHERE d.project = :project
             AND d.step = :step
             AND d.section = :section
             ORDER BY d.name')
        ->setParameter('project', $project)
        ->setParameter('step', $step)
        ->setParameter('section', $section);

        $documents = $documentsQuery->getResult();

        $notesQuery = $em->createQuery(
            'SELECT n
             FROM AppBundle:ProjectNote n
             WHERE n.project = :project
             AND n.step = :step
             AND n.section = :section
             ORDER BY n.created')
        ->setParameter('project', $project)
        ->setParameter('step', $step)
        ->setParameter('section', $section);

        $notes = $notesQuery->getResult();

        $databasePriorityList = null;
        $databaseList = null;
        if ($loadDatabaseList) {
            $priorityByCollegeQuery = $em->createQuery(
                'SELECT l
                FROM AppBundle:LibraryResource l
                JOIN l.colleges c
                WHERE c.college = :c AND c.priority = true
                ORDER BY l.title')
            ->setParameter('c', $project->getDepartment()->getCollege());
            $regularByCollegeQuery = $em->createQuery(
                'SELECT l
                FROM AppBundle:LibraryResource l
                JOIN l.colleges c
                WHERE c.college = :c AND c.priority = false
                ORDER BY l.title')
            ->setParameter('c', $project->getDepartment()->getCollege());
            $priorityByDepartmentQuery = $em->createQuery(
                'SELECT l
                FROM AppBundle:LibraryResource l
                JOIN l.departments d
                WHERE d.department = :d AND d.priority = true
                ORDER BY l.title')
            ->setParameter('d', $project->getDepartment());
            $regularByDepartmentQuery = $em->createQuery(
                'SELECT l
                FROM AppBundle:LibraryResource l
                JOIN l.departments d
                WHERE d.department = :d AND d.priority = true
                ORDER BY l.title')
            ->setParameter('d', $project->getDepartment());

            $collegePriority = $priorityByCollegeQuery->getResult();
            $collegeRegular = $regularByCollegeQuery->getResult();
            $departmentPriority = $priorityByDepartmentQuery->getResult();
            $departmentRegular = $regularByDepartmentQuery->getResult();

            $databasePriorityList = array_merge($collegePriority, $departmentPriority);
            $databaseList = array_merge($collegeRegular, $departmentRegular);

            uasort($databasePriorityList, function($a, $b) { return strnatcasecmp($a->getTitle(), $b->getTitle()); });
            uasort($databaseList, function($a, $b) { return strnatcasecmp($a->getTitle(), $b->getTitle()); });
        }

        // Find the next and previous step sections to go to
        // NOTE: This is based on our current knowledge of the system,
        // where we have a fixed number of steps and sections. We should
        // change this to softcode it, in case this changes.
        // TODO: Compute this algorithmically
        if ($step->getOrdering() == 1) {
            $priorStep = null;
            $nextStep = $em->getRepository('AppBundle:Step')->findOneByOrdering(2);
        } elseif ($step->getOrdering() == 12) {
            $priorStep = $em->getRepository('AppBundle:Step')->findOneByOrdering(11);
            $nextStep = null;
        } else {
            $priorStep = $em->getRepository('AppBundle:Step')->findOneByOrdering($step->getOrdering()-1);
            $nextStep = $em->getRepository('AppBundle:Step')->findOneByOrdering($step->getOrdering()+1);
        }

        if ($section->getOrdering() == 1) {
            $nextStep = null; // TODO: Move this computation so we don't hit the DB
            if (isset($priorStep)) {
                $priorSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(6);
                $nextSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(2);
            } else {
                $priorSection = null;
                $nextSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(2);
            }
        } elseif ($section->getOrdering() == 6) {
            $priorStep = null; // TODO: Move this computation so we don't hit the DB
            if (isset($nextStep)) {
                $priorSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(5);
                $nextSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(1);
            } else {
                $priorSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering(5);
                $nextSection = null;
            }
        } else {
            $nextStep = null; // TODO: Move this computation so we don't hit the DB
            $priorStep = null; // TODO: Move this computation so we don't hit the DB
            $priorSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering($section->getOrdering()-1);
            $nextSection = $em->getRepository('AppBundle:StepSection')->findOneByOrdering($section->getOrdering()+1);
        }

        return array(
            'entity'      => $project,
            'steps'       => $steps,
            'layout'      => $layout,
            'layoutItems' => $layoutItems,
            'form'        => $form->createView(),
            'documents'   => $documents,
            'notes'        => $notes,
            'priorStep'    => $priorStep,
            'nextStep'     => $nextStep,
            'priorSection' => $priorSection,
            'nextSection'  => $nextSection,
            'dbPriority'   => $databasePriorityList,
            'dbList'       => $databaseList,
        );    
    }
}
