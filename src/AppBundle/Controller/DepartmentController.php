<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Department;
use AppBundle\Form\DepartmentType;
use AppBundle\Entity\LibraryResource;
use AppBundle\Entity\LibraryResourceDepartment;
use AppBundle\Entity\ContactRoleMapping;
use AppBundle\Entity\Role;
use AppBundle\Utils\Status;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Department controller.
 *
 * @Route("/admin/department")
 */
class DepartmentController extends Controller
{

    /**
     * Lists all Department entities.
     *
     * @Route("/", name="department")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT d,c FROM AppBundle:Department d JOIN d.college c order by d.name ";
        $query = $em->createQuery($dql);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );
       // $entities = $em->getRepository('AppBundle:Department')->findBy(array(), array('name'=>'asc'));
        // return $this->render('AppBundle:Department:index.html.twig', array('pagination' => $pagination));

       return array(
            'pagination' => $pagination,
        );
    }
    /**
     * Creates a new Department entity.
     *
     * @Route("/", name="department_create")
     * @Method("POST")
     * @Template("AppBundle:Department:new.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createAction(Request $request)
    {
        $entity = new Department();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('department_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Department entity.
     *
     * @param Department $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Department $entity)
    {
        $form = $this->createForm(new DepartmentType(), $entity, array(
            'action' => $this->generateUrl('department_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Department entity.
     *
     * @Route("/new", name="department_new")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction()
    {
        $entity = new Department();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Department entity.
     *
     * @Route("/{id}", name="department_show")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $department = $em->getRepository('AppBundle:Department')->find($id);

        if (!$department) {
            throw $this->createNotFoundException('Unable to find Department entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $priorityByCollegeQuery = $em->createQuery(
            'SELECT lc, l
            FROM AppBundle:LibraryResourceCollege lc
            JOIN lc.resource l
            WHERE lc.college = :c AND lc.priority = true
            ORDER BY l.title')
        ->setParameter('c', $department->getCollege());
        $regularByCollegeQuery = $em->createQuery(
            'SELECT lc, l
            FROM AppBundle:LibraryResourceCollege lc
            JOIN lc.resource l
            WHERE lc.college = :c AND lc.priority = false
            ORDER BY l.title')
        ->setParameter('c', $department->getCollege());

        $priorityByDepartmentQuery = $em->createQuery(
            'SELECT ld, l
            FROM AppBundle:LibraryResourceDepartment ld
            JOIN ld.resource l
            WHERE ld.department = :d AND ld.priority = true
            ORDER BY l.title')
        ->setParameter('d', $department);
        $regularByDepartmentQuery = $em->createQuery(
            'SELECT ld, l
            FROM AppBundle:LibraryResourceDepartment ld
            JOIN ld.resource l
            WHERE ld.department = :d AND ld.priority = false
            ORDER BY l.title')
        ->setParameter('d', $department);

        $collegePriority = $priorityByCollegeQuery->getResult();
        $collegeRegular = $regularByCollegeQuery->getResult();
        $departmentPriority = $priorityByDepartmentQuery->getResult();
        $departmentRegular = $regularByDepartmentQuery->getResult();

        $databasePriorityList = array_merge($collegePriority, $departmentPriority);
        $databaseList = array_merge($collegeRegular, $departmentRegular);

        uasort($departmentPriority, function($a, $b) { return strnatcasecmp($a->getResource()->getTitle(), $b->getResource()->getTitle()); });
        uasort($departmentRegular, function($a, $b) { return strnatcasecmp($a->getResource()->getTitle(), $b->getResource()->getTitle()); });
        uasort($databasePriorityList, function($a, $b) { return strnatcasecmp($a->getResource()->getTitle(), $b->getResource()->getTitle()); });
        uasort($databaseList, function($a, $b) { return strnatcasecmp($a->getResource()->getTitle(), $b->getResource()->getTitle()); });

        $contactsInRolesByCollegeQuery = $em->createQuery(
            'SELECT m, r
            FROM AppBundle:ContactRoleMapping m
            JOIN m.role r
            WHERE :c MEMBER OF m.colleges')
        ->setParameter('c', $department->getCollege());

        $collegeRoles = $contactsInRolesByCollegeQuery->getResult();

        $contactsInRolesByDepartmentQuery = $em->createQuery(
            'SELECT m, r
            FROM AppBundle:ContactRoleMapping m
            JOIN m.role r
            WHERE :d MEMBER OF m.departments')
        ->setParameter('d', $department);

        $departmentRoles = $contactsInRolesByDepartmentQuery->getResult();
        $roles = array_merge($collegeRoles, $departmentRoles);
        uasort($roles, function($a, $b) { return strnatcasecmp($a->getRole()->getRoleName(), $b->getRole()->getRoleName()); });

        return array(
            'entity'       => $department,
            'delete_form'  => $deleteForm->createView(),
            'deptPriority' => $departmentPriority,
            'deptList'     => $departmentRegular,
            'dbPriority'   => $databasePriorityList,
            'dbList'       => $databaseList,
            'roles'        => $roles            
        );
    }

    /**
     * Displays a form to edit an existing Department entity.
     *
     * @Route("/{id}/edit", name="department_edit")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Department')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Department entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Department entity.
    *
    * @param Department $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Department $entity)
    {
        $form = $this->createForm(new DepartmentType(), $entity, array(
            'action' => $this->generateUrl('department_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Department entity.
     *
     * @Route("/{id}", name="department_update")
     * @Method("PUT")
     * @Template("AppBundle:Department:edit.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Department')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Department entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entity->setModified( new \DateTime() );
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('department_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Department entity.
     *
     * @Route("/{id}", name="department_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Department')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Department entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('department'));
    }

    /**
     * Creates a form to delete a Department entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('department_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Remove a library resource linked to this department.
     *
     * @Route("/{id}/{lrid}/removedlr", name="remove_department_library_resource")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removeLibraryResourceAction($id,$lrid)
    {
        $em = $this->getDoctrine()->getManager();

        $department = $em->getRepository('AppBundle:Department')->find($id);

        if (!$department) {
            throw $this->createNotFoundException('Unable to find Department entity.');
        }

        $mapping = $em->getRepository('AppBundle:LibraryResourceDepartment')->find($lrid);

        if (!$mapping) {
            throw $this->createNotFoundException('Unable to find mapping to library resource.');
        }

        $em->remove($mapping);
        $em->flush();

        return $this->redirect($this->generateUrl('department_show', array('id' => $department->getId())));
    }

    /**
     * Toggle the priority of a library resource linked to this department.
     *
     * @Route("/{id}/{lrid}/toggledlr", name="toggle_department_library_resource_priority")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function toggleLibraryResourcePriorityAction($id,$lrid)
    {
        $em = $this->getDoctrine()->getManager();

        $department = $em->getRepository('AppBundle:Department')->find($id);

        if (!$department) {
            throw $this->createNotFoundException('Unable to find Department entity.');
        }

        $mapping = $em->getRepository('AppBundle:LibraryResourceDepartment')->find($lrid);

        if (!$mapping) {
            throw $this->createNotFoundException('Unable to find mapping to library resource.');
        }

        $mapping->setPriority(!$mapping->getPriority());
        $em->persist($mapping);
        $em->flush();

        return $this->redirect($this->generateUrl('department_show', array('id' => $department->getId())));
    }

    /**
     * Show library resources to add for this department.
     *
     * @Route("/{id}/adddlr", name="add_department_library_resource")
     * @Method("GET")
     * @Template("AppBundle:Department:addResource.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addLibraryResourceAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $department = $em->getRepository('AppBundle:Department')->find($id);

        if (!$department) {
            throw $this->createNotFoundException('Unable to find Department entity.');
        }

        $resources = $em->getRepository('AppBundle:LibraryResource')->findAll();

        return array(
            'department'   => $department,
            'resources'    => $resources,
        );

    }

    /**
     * Add a library resource linked to this department.
     *
     * @Route("/{id}/{lrid}/insertdlr", name="insert_department_library_resource")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function insertLibraryResourceAction($id,$lrid)
    {
        $em = $this->getDoctrine()->getManager();

        $department = $em->getRepository('AppBundle:Department')->find($id);

        if (!$department) {
            throw $this->createNotFoundException('Unable to find Department entity.');
        }

        $resource = $em->getRepository('AppBundle:LibraryResource')->find($lrid);

        if (!$resource) {
            throw $this->createNotFoundException('Unable to find library resource entity.');
        }

        $mapping = new LibraryResourceDepartment();
        $mapping->setDepartment($department);
        $mapping->setResource($resource);

        $em->persist($mapping);
        $em->flush();

        return $this->redirect($this->generateUrl('department_show', array('id' => $department->getId())));
    }

}
