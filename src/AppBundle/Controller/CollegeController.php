<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\College;
use AppBundle\Form\CollegeType;
use AppBundle\Entity\LibraryResource;
use AppBundle\Entity\LibraryResourceCollege;
use AppBundle\Entity\ContactRoleMapping;
use AppBundle\Entity\Role;
use AppBundle\Utils\Status;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * College controller.
 *
 * @Route("/admin/college")
 */
class CollegeController extends Controller
{

    /**
     * Lists all College entities.
     *
     * @Route("/", name="college")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:College')->findBy(array(), array('name'=>'asc'));

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new College entity.
     *
     * @Route("/", name="college_create")
     * @Method("POST")
     * @Template("AppBundle:College:new.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createAction(Request $request)
    {
        $entity = new College();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('college_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a College entity.
     *
     * @param College $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(College $entity)
    {
        $form = $this->createForm(new CollegeType(), $entity, array(
            'action' => $this->generateUrl('college_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new College entity.
     *
     * @Route("/new", name="college_new")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction()
    {
        $entity = new College();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a College entity.
     *
     * @Route("/{id}", name="college_show")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:College')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find College entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $priorityByCollegeQuery = $em->createQuery(
            'SELECT lc, l
            FROM AppBundle:LibraryResourceCollege lc
            JOIN lc.resource l
            WHERE lc.college = :c AND lc.priority = true
            ORDER BY l.title')
        ->setParameter('c', $entity);
        $regularByCollegeQuery = $em->createQuery(
            'SELECT lc, l
            FROM AppBundle:LibraryResourceCollege lc
            JOIN lc.resource l
            WHERE lc.college = :c AND lc.priority = false
            ORDER BY l.title')
        ->setParameter('c', $entity);

        $databasePriorityList = $priorityByCollegeQuery->getResult();
        $databaseList = $regularByCollegeQuery->getResult();

        uasort($databasePriorityList, function($a, $b) { return strnatcasecmp($a->getResource()->getTitle(), $b->getResource()->getTitle()); });
        uasort($databaseList, function($a, $b) { return strnatcasecmp($a->getResource()->getTitle(), $b->getResource()->getTitle()); });

        $contactsInRolesByCollegeQuery = $em->createQuery(
            'SELECT m, r
            FROM AppBundle:ContactRoleMapping m
            JOIN m.role r
            WHERE :c MEMBER OF m.colleges')
        ->setParameter('c', $entity);

        $contactRoles = $contactsInRolesByCollegeQuery->getResult();

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'departments' => $entity->getDepartments(),
            'dbPriority'  => $databasePriorityList,
            'dbList'      => $databaseList,
            'roles'       => $contactRoles
        );
    }

    /**
     * Displays a form to edit an existing College entity.
     *
     * @Route("/{id}/edit", name="college_edit")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:College')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find College entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a College entity.
    *
    * @param College $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(College $entity)
    {
        $form = $this->createForm(new CollegeType(), $entity, array(
            'action' => $this->generateUrl('college_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing College entity.
     *
     * @Route("/{id}", name="college_update")
     * @Method("PUT")
     * @Template("AppBundle:College:edit.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:College')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find College entity.');
        }


        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entity->setModified( new \DateTime() );
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('college_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a College entity.
     *
     * @Route("/{id}", name="college_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:College')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find College entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('college'));
    }

    /**
     * Creates a form to delete a College entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('college_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Remove a library resource linked to this college.
     *
     * @Route("/{id}/{lrid}/removeclr", name="remove_college_library_resource")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removeLibraryResourceAction($id,$lrid)
    {
        $em = $this->getDoctrine()->getManager();

        $college = $em->getRepository('AppBundle:College')->find($id);

        if (!$college) {
            throw $this->createNotFoundException('Unable to find College entity.');
        }

        $mapping = $em->getRepository('AppBundle:LibraryResourceCollege')->find($lrid);

        if (!$mapping) {
            throw $this->createNotFoundException('Unable to find mapping to library resource.');
        }

        $em->remove($mapping);
        $em->flush();

        return $this->redirect($this->generateUrl('college_show', array('id' => $college->getId())));
    }

    /**
     * Toggle the priority of a library resource linked to this college.
     *
     * @Route("/{id}/{lrid}/toggleclr", name="toggle_college_library_resource_priority")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function toggleLibraryResourcePriorityAction($id,$lrid)
    {
        $em = $this->getDoctrine()->getManager();

        $college = $em->getRepository('AppBundle:College')->find($id);

        if (!$college) {
            throw $this->createNotFoundException('Unable to find College entity.');
        }

        $mapping = $em->getRepository('AppBundle:LibraryResourceCollege')->find($lrid);

        if (!$mapping) {
            throw $this->createNotFoundException('Unable to find mapping to library resource.');
        }

        $mapping->setPriority(!$mapping->getPriority());
        $em->persist($mapping);
        $em->flush();

        return $this->redirect($this->generateUrl('college_show', array('id' => $college->getId())));
    }

    /**
     * Show library resources to add for this college.
     *
     * @Route("/{id}/addclr", name="add_college_library_resource")
     * @Method("GET")
     * @Template("AppBundle:College:addResource.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addLibraryResourceAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $college = $em->getRepository('AppBundle:College')->find($id);

        if (!$college) {
            throw $this->createNotFoundException('Unable to find College entity.');
        }

        $resources = $em->getRepository('AppBundle:LibraryResource')->findAll();

        return array(
            'college'      => $college,
            'resources'    => $resources,
        );

    }

    /**
     * Add a library resource linked to this college.
     *
     * @Route("/{id}/{lrid}/insertclr", name="insert_college_library_resource")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function insertLibraryResourceAction($id,$lrid)
    {
        $em = $this->getDoctrine()->getManager();

        $college = $em->getRepository('AppBundle:College')->find($id);

        if (!$college) {
            throw $this->createNotFoundException('Unable to find College entity.');
        }

        $resource = $em->getRepository('AppBundle:LibraryResource')->find($lrid);

        if (!$resource) {
            throw $this->createNotFoundException('Unable to find library resource entity.');
        }

        $mapping = new LibraryResourceCollege();
        $mapping->setCollege($college);
        $mapping->setResource($resource);

        $em->persist($mapping);
        $em->flush();

        return $this->redirect($this->generateUrl('college_show', array('id' => $college->getId())));
    }

}
