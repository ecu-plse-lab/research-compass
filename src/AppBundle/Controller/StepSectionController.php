<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\StepSection;
use AppBundle\Form\StepSectionType;
use AppBundle\Utils\Status;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * StepSection controller.
 *
 * @Route("/admin/stepsection")
 */
class StepSectionController extends Controller
{

    /**
     * Lists all StepSection entities.
     *
     * @Route("/", name="stepsection")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:StepSection')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new StepSection entity.
     *
     * @Route("/", name="stepsection_create")
     * @Method("POST")
     * @Template("AppBundle:StepSection:new.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createAction(Request $request)
    {
        $entity = new StepSection();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('stepsection_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a StepSection entity.
     *
     * @param StepSection $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(StepSection $entity)
    {
        $form = $this->createForm(new StepSectionType(), $entity, array(
            'action' => $this->generateUrl('stepsection_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new StepSection entity.
     *
     * @Route("/new", name="stepsection_new")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction()
    {
        $entity = new StepSection();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a StepSection entity.
     *
     * @Route("/{id}", name="stepsection_show")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:StepSection')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StepSection entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'layouts'     => $entity->getLayouts(),            
        );
    }

    /**
     * Displays a form to edit an existing StepSection entity.
     *
     * @Route("/{id}/edit", name="stepsection_edit")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:StepSection')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StepSection entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a StepSection entity.
    *
    * @param StepSection $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(StepSection $entity)
    {
        $form = $this->createForm(new StepSectionType(), $entity, array(
            'action' => $this->generateUrl('stepsection_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing StepSection entity.
     *
     * @Route("/{id}", name="stepsection_update")
     * @Method("PUT")
     * @Template("AppBundle:StepSection:edit.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:StepSection')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StepSection entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entity->setModified( new \DateTime() );
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('stepsection_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a StepSection entity.
     *
     * @Route("/{id}", name="stepsection_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:StepSection')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find StepSection entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('stepsection'));
    }

    /**
     * Creates a form to delete a StepSection entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('stepsection_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
