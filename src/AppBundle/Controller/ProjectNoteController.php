<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\ProjectNote;
use AppBundle\Form\ProjectNoteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Utils\SharingStatus;

/**
 * ProjectNote controller.
 *
 * @Route("/projectnote")
 */
class ProjectNoteController extends Controller
{
    // NOTE: These are copied from ProjectController, and should be
    // moved into a utility class....
    private function userIsOwner($project, $user) 
    {
        return ($project->getOwner() == $user);
    }

    private function userIsCollaborator($project, $user)
    {
        $em = $this->getDoctrine()->getManager();
        $projectQuery = $em->createQuery(
            'SELECT p, sp 
             FROM AppBundle:Project p
             JOIN p.sharedProjects sp
             WHERE p.id = :id AND sp.user = :user AND sp.accepted = :status')
        ->setParameter('id', $project->getId())
        ->setParameter('user', $user)
        ->setParameter('status', SharingStatus::ACCEPTED);

        $projects = $projectQuery->getResult();
        if (count($projects) == 1) {
            return true;
        }

        return false;
    }

    private function userIsOwnerOrCollaborator($project, $user)
    {
        if ($this->userIsOwner($project, $user)) {
            return true;
        }

        if ($this->userIsCollaborator($project, $user))
        {
            return true;
        }

        return false;
    }

    /**
     * Lists all ProjectNote entities.
     *
     * @Route("/", name="projectnote")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:ProjectNote')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new ProjectNote entity.
     *
     * @Route("/create/{projectid}/{stepid}/{sectionid}", name="projectnote_create")
     * @Method("POST")
     * @Template("AppBundle:ProjectNote:new.html.twig")
     * @Security("has_role('ROLE_USER')")
     */
    public function createAction(Request $request, $projectid, $stepid, $sectionid)
    {
        // The user must be logged in to perform this step, we may be able to
        // remove this check since we have a role check in the annotation above...
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back the given project
        $project = $em->getRepository('AppBundle:Project')->find($projectid);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        // Make sure this user has access to work on this project, they cannot add
        // a document to it otherwise
        if (! $this->userIsOwnerOrCollaborator($project, $user)) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Project entity.');   
        }

        // Get back the given step
        $step = $em->getRepository('AppBundle:Step')->find($stepid);
        if (!$step) {
            throw $this->createNotFoundException('Unable to find Step entity.');
        }

        // Get back the given section
        $section = $em->getRepository('AppBundle:StepSection')->find($sectionid);
        if (!$section) {
            throw $this->createNotFoundException('Unable to find Section entity.');
        }
        
        $entity = new ProjectNote();
        $entity->setProject($project);
        $entity->setStep($step);
        $entity->setSection($section);
        $form = $this->createCreateForm($entity, $projectid, $stepid, $sectionid);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('project_section',
                array('id' => $projectid, 'stepid' => $stepid,
                      'sectionid' => $sectionid)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a ProjectNote entity.
     *
     * @param ProjectNote $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ProjectNote $entity, $projectid, $stepid, $sectionid)
    {
        $form = $this->createForm(new ProjectNoteType(), $entity, array(
            'action' => $this->generateUrl('projectnote_create',
                array('projectid' => $projectid, 'stepid' => $stepid,
                      'sectionid' => $sectionid)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create','disabled'=>true));

        return $form;
    }

    /**
     * Displays a form to create a new ProjectNote entity.
     *
     * @Route("/new/{projectid}/{stepid}/{sectionid}", name="projectnote_new")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function newAction($projectid, $stepid, $sectionid)
    {
        // The user must be logged in to perform this step, we may be able to
        // remove this check since we have a role check in the annotation above...
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back the given project
        $project = $em->getRepository('AppBundle:Project')->find($projectid);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        // Make sure this user has access to work on this project, they cannot add
        // a document to it otherwise
        if (! $this->userIsOwnerOrCollaborator($project, $user)) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Project entity.');   
        }

        // Get back the given step
        $step = $em->getRepository('AppBundle:Step')->find($stepid);
        if (!$step) {
            throw $this->createNotFoundException('Unable to find Step entity.');
        }

        // Get back the given section
        $section = $em->getRepository('AppBundle:StepSection')->find($sectionid);
        if (!$section) {
            throw $this->createNotFoundException('Unable to find Section entity.');
        }

        $entity = new ProjectNote();
        $entity->setProject($project);
        $entity->setStep($step);
        $entity->setSection($section);
        $form   = $this->createCreateForm($entity, $projectid, $stepid, $sectionid);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a ProjectNote entity.
     *
     * @Route("/{id}", name="projectnote_show")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ProjectNote')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProjectNote entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ProjectNote entity.
     *
     * @Route("/{id}/edit", name="projectnote_edit")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ProjectNote')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProjectNote entity.');
        }

        // The user must be logged in to perform this step, we may be able to
        // remove this check since we have a role check in the annotation above...
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back the given project
        $project = $entity->getProject();

        // Make sure this user has access to work on this project, they cannot add
        // a document to it otherwise
        if (! $this->userIsOwnerOrCollaborator($project, $user)) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Project entity.');   
        }

        $editForm = $this->createEditForm($entity);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
    * Creates a form to edit a ProjectNote entity.
    *
    * @param ProjectNote $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ProjectNote $entity)
    {
        $form = $this->createForm(new ProjectNoteType(), $entity, array(
            'action' => $this->generateUrl('projectnote_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update', 'disabled' => true));

        return $form;
    }
    /**
     * Edits an existing ProjectNote entity.
     *
     * @Route("/{id}", name="projectnote_update")
     * @Method("PUT")
     * @Template("AppBundle:ProjectNote:edit.html.twig")
     * @Security("has_role('ROLE_USER')")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ProjectNote')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProjectNote entity.');
        }

        // The user must be logged in to perform this step, we may be able to
        // remove this check since we have a role check in the annotation above...
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        // Get back the given project
        $project = $entity->getProject();

        // Make sure this user has access to work on this project, they cannot add
        // a document to it otherwise
        if (! $this->userIsOwnerOrCollaborator($project, $user)) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Project entity.');   
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('project_section', 
                array('id' => $project->getId(),
                      'stepid' => $entity->getStep()->getId(),
                      'sectionid' => $entity->getSection()->getId())));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }
    /**
     * Deletes a ProjectNote entity.
     *
     * @Route("/delete/{id}", name="projectnote_delete")
     * @Method({"GET","DELETE"})
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteAction(Request $request, $id)
    {
        // The user must be logged in to perform this step, we may be able to
        // remove this check since we have a role check in the annotation above...
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        // Get back the user, based on the username of the signed-in user
        $securityUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($securityUser->getUsername());

        $entity = $em->getRepository('AppBundle:ProjectNote')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProjectNote entity.');
        }

        // Get back the given project
        $project = $entity->getProject();
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        // Make sure this user has access to work on this project, they cannot add
        // a document to it otherwise
        if (! $this->userIsOwnerOrCollaborator($project, $user)) {
            // TODO: Throw a more appropriate error message!
            throw $this->createNotFoundException('Unable to find Project entity.');   
        }

        // Get back the given step
        $step = $entity->getStep();
        $section = $entity->getSection();

        $em->remove($entity);
        $em->flush();

        $flash = $this->get('braincrafted_bootstrap.flash');
        $flash->success('Note deleted.');            

        return $this->redirect($this->generateUrl('project_section',
                array('id' => $project->getId(), 'stepid' => $step->getId(),
                      'sectionid' => $section->getId())));
    }
}
