<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\ContactRoleMapping;
use AppBundle\Form\ContactRoleMappingType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * ContactRoleMapping controller.
 *
 * @Route("/admin/contactrolemapping")
 */
class ContactRoleMappingController extends Controller
{

    /**
     * Lists all ContactRoleMapping entities.
     *
     * @Route("/", name="contactrolemapping")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:ContactRoleMapping')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new ContactRoleMapping entity.
     *
     * @Route("/", name="contactrolemapping_create")
     * @Method("POST")
     * @Template("AppBundle:ContactRoleMapping:new.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createAction(Request $request)
    {
        $entity = new ContactRoleMapping();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('contactrolemapping_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a ContactRoleMapping entity.
     *
     * @param ContactRoleMapping $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ContactRoleMapping $entity)
    {
        $form = $this->createForm(new ContactRoleMappingType(), $entity, array(
            'action' => $this->generateUrl('contactrolemapping_create'),
            'method' => 'POST',
            'em'     => $this->getDoctrine()->getManager(),
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ContactRoleMapping entity.
     *
     * @Route("/new/{roleId}", name="contactrolemapping_new")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction($roleId)
    {
        $entity = new ContactRoleMapping();

        $em = $this->getDoctrine()->getManager();
        $role = $em->getRepository('AppBundle:Role')->find($roleId);

        if (!$role) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $entity->setRole($role);

        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Finds and displays a ContactRoleMapping entity.
     *
     * @Route("/{id}", name="contactrolemapping_show")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ContactRoleMapping')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactRoleMapping entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ContactRoleMapping entity.
     *
     * @Route("/{id}/edit", name="contactrolemapping_edit")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ContactRoleMapping')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactRoleMapping entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a ContactRoleMapping entity.
    *
    * @param ContactRoleMapping $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ContactRoleMapping $entity)
    {
        $form = $this->createForm(new ContactRoleMappingType(), $entity, array(
            'action' => $this->generateUrl('contactrolemapping_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'em'     => $this->getDoctrine()->getManager(),        
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ContactRoleMapping entity.
     *
     * @Route("/{id}", name="contactrolemapping_update")
     * @Method("PUT")
     * @Template("AppBundle:ContactRoleMapping:edit.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ContactRoleMapping')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactRoleMapping entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('contactrolemapping_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a ContactRoleMapping entity.
     *
     * @Route("/{id}", name="contactrolemapping_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:ContactRoleMapping')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ContactRoleMapping entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('contactrolemapping'));
    }

    /**
     * Creates a form to delete a ContactRoleMapping entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contactrolemapping_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
