<?php

namespace AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Role;

class RoleToRoleNameTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (role) to a string (role name).
     *
     * @param  Role|null $role
     * @return string
     */
    public function transform($role)
    {
        if (null === $role) {
            return "";
        }

        return $role->getRoleName();
    }

    /**
     * Transforms a string (role name) to an object (role).
     *
     * @param  string $roleName
     *
     * @return Role|null
     *
     * @throws TransformationFailedException if object (role) is not found.
     */
    public function reverseTransform($roleName)
    {
        if (!$roleName) {
            return null;
        }

        $role = $this->om
            ->getRepository('AppBundle:Role')
            ->findOneBy(array('roleName' => $roleName))
        ;

        if (null === $role) {
            throw new TransformationFailedException(sprintf(
                'A role with role name "%s" does not exist!',
                $roleName
            ));
        }

        return $role;
    }
}