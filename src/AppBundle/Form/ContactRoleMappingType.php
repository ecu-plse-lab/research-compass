<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AppBundle\Form\DataTransformer\RoleToRoleNameTransformer;

class ContactRoleMappingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];
        $transformer = new RoleToRoleNameTransformer($entityManager);

        $builder
            ->add($builder->create('role', 'text')->addModelTransformer($transformer))
            ->add('contacts')
            ->add('colleges')
            ->add('departments')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ContactRoleMapping'
        ))
        ->setRequired(array('em'))
        ->setAllowedTypes('em', 'Doctrine\Common\Persistence\ObjectManager')
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_contactrolemapping';
    }
}
