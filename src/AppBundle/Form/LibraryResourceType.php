<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LibraryResourceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('givenId')
            ->add('title')
            ->add('description')
            ->add('libraryLink')
            ->add('directLink')
            ->add('format')
            ->add('tag')
            ->add('subjects')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\LibraryResource'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_libraryresource';
    }
}
