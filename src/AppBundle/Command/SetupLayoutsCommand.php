<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Entity\Step;
use AppBundle\Entity\StepSection;
use AppBundle\Entity\StaticItem;
use AppBundle\Entity\Layout;
use AppBundle\Entity\StaticLayoutItem;

class SetupLayoutsCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('compass:setup:layouts')
			->setDescription('Setup basic layouts for each step and section');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$em = $this->getContainer()->get('doctrine')->getManager();
		$stepRepository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Step');
		$stepSectionRepository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:StepSection');

		$steps = $stepRepository->findAll();
		$sections = $stepSectionRepository->findAll();

		foreach ($steps as $step) {
			$output->writeln('Setting up layouts for step ' . $step->getName());
			foreach ($sections as $section) {
				$output->writeln('Setting up layouts for section ' . $section->getName());

				$layout = new Layout();
				$layout->setStep($step);
				$layout->setStepSection($section);
				$em->persist($layout);
				$em->flush();				

				$order = 1;
				$items = $step->getStaticItems();

				foreach ($items as $item) {
					if ($item->getStepSection() == $section) {
						$layoutItem = new StaticLayoutItem();
						$layoutItem->setOrder($order++);
						$layoutItem->setStaticItem($item);
						$layoutItem->setLayout($layout);
						$layout->addLayoutItem($layoutItem);

						$em->persist($layout);
						$em->persist($layoutItem);
						$em->flush();				
					}
				}

				$output->writeln('Added ' . ($order-1) . ' items to layout');
			}
		}
	}
}