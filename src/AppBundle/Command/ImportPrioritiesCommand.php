<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Entity\College;
use AppBundle\Entity\Department;
use AppBundle\Entity\LibraryResource;
use AppBundle\Entity\LibraryResourceCollege;
use AppBundle\Entity\LibraryResourceDepartment;

class ImportPrioritiesCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('compass:import:priorities')
			->setDescription('Import information on library resource priorities into Research Compass')
			->addArgument(
				'path',
				InputArgument::REQUIRED,
				'Where is the csv file to load?'
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$inputPath = $input->getArgument('path');
		$em = $this->getContainer()->get('doctrine')->getManager();
		$resourceRepo = $this->getContainer()->get('doctrine')->getRepository('AppBundle:LibraryResource');
		$resourceCollegeRepo = $this->getContainer()->get('doctrine')->getRepository('AppBundle:LibraryResourceCollege');
		$resourceDepartmentRepo = $this->getContainer()->get('doctrine')->getRepository('AppBundle:LibraryResourceDepartment');
		$collegeRepo = $this->getContainer()->get('doctrine')->getRepository('AppBundle:College');
		$departmentRepo = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Department');

		if ( ($handle = fopen($inputPath, 'r')) !== FALSE ) {
			$output->writeln('Loading resource priority information from file ' . $inputPath);
			while ($data = fgetcsv($handle, 0)) {
				$priorityInfo[] = $data;
			}
			fclose($handle);

			$output->writeln('Found ' . count($priorityInfo) . ' library resource priority records');

			$counter = 1;

			foreach($priorityInfo as $priorityRec) {
				$counter += 1;
				$collegeName = trim($priorityRec[0]);
				$departmentName = trim($priorityRec[1]);
				$resourceName = trim($priorityRec[2]);
				$priority = trim($priorityRec[3]);

				$resource = $resourceRepo->findOneByTitle($resourceName);
				if (!$resource && strlen($resourceName) > 0) {
					$output->writeln('Invalid resource title given: ' . $resourceName . ', line ' . $counter);
				} elseif ($resource) {
					$college = null;
					$department = null;

					if (strlen($collegeName) > 0) {
						$college = $collegeRepo->findOneByName($collegeName);
						if (!$college) {
							$output->writeln('Invalid college given:' . $collegeName);
						}						
					}

					if (strlen($departmentName) > 0) {
						$department = $departmentRepo->findOneByName($departmentName);
						if (!$department) {
							$output->writeln('Invalid department given:' . $departmentName);
						}
					}

					if (($college && $department) || $department) {
						$foundIt = false;
						foreach($resource->getDepartments() as $resourceDepartment) {
							if ($resourceDepartment->getDepartment() == $department) {
								$foundIt = true;
								$resourceDepartment->setPriority($priority == 'Y');
								$em->persist($resourceDepartment);
								$em->flush();
								break;
							}
						}
						if (!$foundIt) {
							$resourceDepartment = new LibraryResourceDepartment();
							$resourceDepartment->setResource($resource);
							$resourceDepartment->setDepartment($department);
							$resourceDepartment->setPriority($priority == 'Y');
							$em->persist($resourceDepartment);
							$resource->addDepartment($resourceDepartment);
							$em->persist($resource);
							$em->flush();
						}
					} elseif ($college) {
						foreach($resource->getColleges() as $resourceCollege) {
							if ($resourceCollege->getCollege() == $college) {
								$foundIt = true;
								$resourceCollege->setPriority($priority == 'Y');
								$em->persist($resourceCollege);
								$em->flush();
								break;
							}
						}
						if (!$foundIt) {
							$resourceCollege = new LibraryResourceCollege();
							$resourceCollege->setResource($resource);
							$resourceCollege->setCollege($college);
							$resourceCollege->setPriority($priority == 'Y');
							$em->persist($resourceCollege);
							$resource->addCollege($resourceCollege);
							$em->persist($resource);
							$em->flush();
						}
					}
				}

			}
		} else {
			$output->writeln('Could not open file ' . $inputPath . ', check to ensure it exists and is readable.');
		}

	}
}