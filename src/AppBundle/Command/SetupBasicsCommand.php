<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Entity\Step;
use AppBundle\Entity\StepSection;

class SetupBasicsCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('compass:setup:basics')
			->setDescription('Setup basic data in the Research Compass tables');
	}

	private function addStep($em, $stepName, $stepNumber)
	{
		$step = new Step();
		$step->setName($stepName);
		$step->setOrdering($stepNumber);
		$em->persist($step); 
		$em->flush();

	}

	private function addStepSection($em, $stepSectionName, $stepSectionNumber)
	{
		$stepSection = new StepSection();
		$stepSection = new StepSection();
		$stepSection->setName($stepSectionName);
		$stepSection->setOrdering($stepSectionNumber);
		$em->persist($stepSection); 
		$em->flush();

	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$em = $this->getContainer()->get('doctrine')->getManager();
		// $stepRepository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Step');
		// $stepSectionRepository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:StepSection');

		// Add Roadmap Steps
		$i = 1;
		$output->writeln("Adding Research Compass Steps");
		$this->addStep($em, "Concept Formation", $i++);
		$this->addStep($em, "Resource Analysis", $i++);
		$this->addStep($em, "Team Development", $i++);
		$this->addStep($em, "Feasibility Study", $i++);
		$this->addStep($em, "Identify/Select Funding Program", $i++);
		$this->addStep($em, "Develop Proposal", $i++);
		$this->addStep($em, "Finalize Proposal", $i++);
		$this->addStep($em, "Submit Proposal", $i++);
		$this->addStep($em, "Proposal Award", $i++);
		$this->addStep($em, "Project Execution", $i++);
		$this->addStep($em, "Project Management", $i++);
		$this->addStep($em, "Project Closeout", $i);

		$i = 1;
		$output->writeln("Adding Research Compass Step Sections");
		$this->addStepSection($em, "Goals", $i++);
		$this->addStepSection($em, "Actions", $i++);
		$this->addStepSection($em, "Connections", $i++);
		$this->addStepSection($em, "Resources", $i++);
		$this->addStepSection($em, "Tools", $i++);
		$this->addStepSection($em, "Outcomes", $i);
	}
}