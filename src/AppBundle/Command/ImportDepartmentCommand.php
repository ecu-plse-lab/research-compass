<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Entity\College;
use AppBundle\Entity\Department;

class ImportDepartmentCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('compass:import:department')
			->setDescription('Import Department names and college mappings into Research Compass')
			->addArgument(
				'path',
				InputArgument::REQUIRED,
				'Where is the csv file to load?'
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$inputPath = $input->getArgument('path');
		$em = $this->getContainer()->get('doctrine')->getManager();
		$departmentRepo = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Department');
		$collegeRepo = $this->getContainer()->get('doctrine')->getRepository('AppBundle:College');

		if ( ($handle = fopen($inputPath, 'r')) !== FALSE ) {
			$output->writeln('Loading department information from file ' . $inputPath);
			while ($data = fgetcsv($handle, 0)) {
				$departmentInfo[] = $data;
			}
			fclose($handle);

			$output->writeln('Found ' . count($departmentInfo) . ' department info records');

			foreach($departmentInfo as $departmentRec) {
				$departmentName = $departmentRec[1];
				$collegeName = $departmentRec[0];
				$college = $collegeRepo->findOneByName($collegeName);
				if (!$college) {
					$output->writeln('Cannot load department ' . $departmentName . ' since college ' . $collegeName . ' does not exist');
				} else {
					$department = $departmentRepo->findOneByName($departmentName);
					if (!$department) {
						$department = new Department();
						$department->setName($departmentName);
						$department->setCollege($college);
						$em->persist($department);
						$em->flush();
						$output->writeln('Created department ' . $departmentName .  ' with id ' . $department->getId());
					} else {
						$output->writeln($departmentName . ' already exists');
					}
				}
			}
		} else {
			$output->writeln('Could not open file ' . $inputPath . ', check to ensure it exists and is readable.');
		}

	}
}