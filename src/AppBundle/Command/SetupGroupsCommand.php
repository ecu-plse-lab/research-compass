<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Entity\Group;

class SetupGroupsCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('compass:setup:groups')
			->setDescription('Setup security group data in the Research Compass tables');
	}

	private function addGroup($em, $groupName)
	{
		$group = new Group();
		$group->setName($groupName);
		$em->persist($group);
		$em->flush();
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$em = $this->getContainer()->get('doctrine')->getManager();

		$this->addGroup($em, 'User');
		$this->addGroup($em, 'Admin');
	}
}