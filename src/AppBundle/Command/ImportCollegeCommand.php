<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Entity\College;

class ImportCollegeCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('compass:import:college')
			->setDescription('Import College names into Research Compass')
			->addArgument(
				'path',
				InputArgument::REQUIRED,
				'Where is the csv file to load?'
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$inputPath = $input->getArgument('path');
		$em = $this->getContainer()->get('doctrine')->getManager();
		$repository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:College');

		if ( ($handle = fopen($inputPath, 'r')) !== FALSE ) {
			$output->writeln('Loading college information from file ' . $inputPath);
			while ($data = fgetcsv($handle, 0)) {
				$collegeNames[] = $data[0];
				$output->writeln("Found college named " . $data[0]);
			}
			fclose($handle);

			$collegeNames = array_unique($collegeNames);
			$output->writeln('Found ' . count($collegeNames) . ' distinct colleges');

			foreach($collegeNames as $collegeName) {
				$college = $repository->findOneByName($collegeName);
				if (!$college) {
					$college = new College();
					$college->setName($collegeName);
					$em->persist($college);
					$em->flush();
					$output->writeln('Created college ' . $collegeName .  ' with id ' . $college->getId());
				} else {
					$output->writeln($collegeName . ' already exists');
				}
			}
		} else {
			$output->writeln('Could not open file ' . $inputPath . ', check to ensure it exists and is readable.');
		}

	}
}