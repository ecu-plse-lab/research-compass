<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Entity\Role;

class ImportRoleCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('compass:import:role')
			->setDescription('Import Role names into Research Compass')
			->addArgument(
				'path',
				InputArgument::REQUIRED,
				'Where is the csv file to load?'
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$inputPath = $input->getArgument('path');
		$em = $this->getContainer()->get('doctrine')->getManager();
		$repository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Role');

		if ( ($handle = fopen($inputPath, 'r')) !== FALSE ) {
			$output->writeln('Loading role information from file ' . $inputPath);
			while ($data = fgetcsv($handle, 0)) {
				$roleNames[] = $data[0];
				$output->writeln("Found role named " . $data[0]);
			}
			fclose($handle);

			$roleNames = array_unique($roleNames);
			$output->writeln('Found ' . count($roleNames) . ' distinct roles');

			foreach($roleNames as $roleName) {
				$role = $repository->findOneByRoleName($roleName);
				if (!$role) {
					$role = new Role();
					$role->setRoleName($roleName);
					$em->persist($role);
					$em->flush();
					$output->writeln('Created role ' . $roleName .  ' with id ' . $role->getId());
				} else {
					$output->writeln($roleName . ' already exists');
				}
			}
		} else {
			$output->writeln('Could not open file ' . $inputPath . ', check to ensure it exists and is readable.');
		}

	}
}