<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Role;
use AppBundle\Entity\Department;
use AppBundle\Entity\College;
use AppBundle\Entity\ContactRoleMapping;

class ImportContactCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('compass:import:contact')
			->setDescription('Import Contacts into Research Compass')
			->addArgument(
				'type',
				InputArgument::REQUIRED,
				'What type of contact list is being imported?'
			)
			->addArgument(
				'path',
				InputArgument::REQUIRED,
				'Where is the csv file to load?'
			)
			;
	}

	protected function importRGS(InputInterface $input, OutputInterface $output)
	{
		$inputPath = $input->getArgument('path');
		$em = $this->getContainer()->get('doctrine')->getManager();
		$repository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Contact');

		if ( ($handle = fopen($inputPath, 'r')) !== FALSE ) {
			$output->writeln('Loading contact information from file ' . $inputPath);
			while ($data = fgetcsv($handle, 0)) {
				$contacts[] = $data;
				$output->writeln("Found contact named " . $data[1]);
			}
			fclose($handle);

			$output->writeln('Found ' . count($contacts) . ' contacts');

			foreach($contacts as $contactItem) {
				$contact = $repository->findOneByDisplayName($contactItem[1]);
				if (!$contact) {
					$contact = new Contact();

					$nameParts = explode(' ', $contactItem[1]);
					if (count($nameParts) === 1) {
						$contact->setLastName($nameParts[0]);
					} elseif (count($nameParts) === 2) {
						$contact->setFirstName($nameParts[0]);
						$contact->setLastName($nameParts[1]);
					} elseif (count($nameParts) === 3) {
						$contact->setFirstName($nameParts[0]);
						$contact->setMiddleName($nameParts[1]);
						$contact->setLastName($nameParts[2]);
					} else {
						$contact->setFirstName($nameParts[0]);
						$middleName = '';
						for ($i = 1; $i < count($nameParts)-1; ++$i) {
							$middleName .= $nameParts[i];
						}
						$contact->setMiddleName($middleName);
						$contact->setLastName($nameParts[count($nameParts)-1]);
					}
					$contact->setDisplayName($contactItem[1]);
					$contact->setContactEmail($contactItem[2]);
					$contact->setContactPhone($contactItem[3]);
					$contact->setOfficeLocation($contactItem[4]);
					$em->persist($contact);
					$em->flush();
					$output->writeln('Created contact ' . $contact->getDisplayName() .  ' with id ' . $contact->getId());
				} else {
					$output->writeln($contact->getDisplayName() . ' already exists');
				}
			}
		} else {
			$output->writeln('Could not open file ' . $inputPath . ', check to ensure it exists and is readable.');
		}		
	}

	protected function importOGC(InputInterface $input, OutputInterface $output)
	{
		$inputPath = $input->getArgument('path');
		$em = $this->getContainer()->get('doctrine')->getManager();
		$repository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Contact');

		if ( ($handle = fopen($inputPath, 'r')) !== FALSE ) {
			$output->writeln('Loading contact information from file ' . $inputPath);
			while ($data = fgetcsv($handle, 0)) {
				$contacts[] = $data;
				$output->writeln("Found contact named " . $data[3]);
			}
			fclose($handle);

			$output->writeln('Found ' . count($contacts) . ' contacts');

			foreach($contacts as $contactItem) {
				$displayName = trim(ucwords(strtolower($contactItem[3])));
				if (strlen($displayName) > 0) {
					$contact = $repository->findOneByDisplayName($displayName);
					if (!$contact) {
						$contact = new Contact();

						$nameParts = explode(' ', $displayName);
						if (count($nameParts) === 1) {
							$contact->setLastName($nameParts[0]);
						} elseif (count($nameParts) === 2) {
							$contact->setFirstName($nameParts[0]);
							$contact->setLastName($nameParts[1]);
						} elseif (count($nameParts) === 3) {
							$contact->setFirstName($nameParts[0]);
							$contact->setMiddleName($nameParts[1]);
							$contact->setLastName($nameParts[2]);
						} else {
							$contact->setFirstName($nameParts[0]);
							$middleName = '';
							for ($i = 1; $i < count($nameParts)-1; ++$i) {
								$middleName .= $nameParts[i];
							}
							$contact->setMiddleName($middleName);
							$contact->setLastName($nameParts[count($nameParts)-1]);
						}
						$contact->setDisplayName($displayName);
						$contact->setContactEmail($contactItem[5]);
						$contact->setContactPhone($contactItem[4]);
						$em->persist($contact);
						$em->flush();
						$output->writeln('Created contact ' . $contact->getDisplayName() .  ' with id ' . $contact->getId());
					} else {
						$output->writeln($contact->getDisplayName() . ' already exists');
					}
				}
			}
		} else {
			$output->writeln('Could not open file ' . $inputPath . ', check to ensure it exists and is readable.');
		}		
	}

	protected function importES(InputInterface $input, OutputInterface $output)
	{
		$inputPath = $input->getArgument('path');
		$em = $this->getContainer()->get('doctrine')->getManager();
		$repository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Contact');

		if ( ($handle = fopen($inputPath, 'r')) !== FALSE ) {
			$output->writeln('Loading contact information from file ' . $inputPath);
			while ($data = fgetcsv($handle, 0)) {
				$contacts[] = $data;
				$output->writeln("Found contact named " . $data[0]);
			}
			fclose($handle);

			$output->writeln('Found ' . count($contacts) . ' contacts');

			foreach($contacts as $contactItem) {
				$contact = $repository->findOneByDisplayName($contactItem[0]);
				if (!$contact) {
					$contact = new Contact();

					$nameParts = explode(' ', $contactItem[0]);
					if (count($nameParts) === 1) {
						$contact->setLastName($nameParts[0]);
					} elseif (count($nameParts) === 2) {
						$contact->setFirstName($nameParts[0]);
						$contact->setLastName($nameParts[1]);
					} elseif (count($nameParts) === 3) {
						$contact->setFirstName($nameParts[0]);
						$contact->setMiddleName($nameParts[1]);
						$contact->setLastName($nameParts[2]);
					} else {
						$contact->setFirstName($nameParts[0]);
						$middleName = '';
						for ($i = 1; $i < count($nameParts)-1; ++$i) {
							$middleName .= $nameParts[i];
						}
						$contact->setMiddleName($middleName);
						$contact->setLastName($nameParts[count($nameParts)-1]);
					}
					$contact->setDisplayName($contactItem[0]);
					$contact->setContactEmail($contactItem[1]);
					$contact->setContactPhone($contactItem[2]);
					$em->persist($contact);
					$em->flush();
					$output->writeln('Created contact ' . $contact->getDisplayName() .  ' with id ' . $contact->getId());
				} else {
					$output->writeln($contact->getDisplayName() . ' already exists');
				}
			}
		} else {
			$output->writeln('Could not open file ' . $inputPath . ', check to ensure it exists and is readable.');
		}		
	}

	protected function importLL(InputInterface $input, OutputInterface $output)
	{
		$inputPath = $input->getArgument('path');
		$em = $this->getContainer()->get('doctrine')->getManager();
		$repository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Contact');

		if ( ($handle = fopen($inputPath, 'r')) !== FALSE ) {
			$output->writeln('Loading contact information from file ' . $inputPath);
			while ($data = fgetcsv($handle, 0)) {
				$contacts[] = $data;
			}
			fclose($handle);

			$output->writeln('Found ' . count($contacts) . ' records');

			// Go through once to add the contact records that are missing
			foreach($contacts as $contactItem) {
				$contact = $repository->findOneByDisplayName($contactItem[2]);
				if (!$contact) {
					$contact = new Contact();

					$nameParts = explode(' ', $contactItem[2]);
					if (count($nameParts) === 1) {
						$contact->setLastName($nameParts[0]);
					} elseif (count($nameParts) === 2) {
						$contact->setFirstName($nameParts[0]);
						$contact->setLastName($nameParts[1]);
					} elseif (count($nameParts) === 3) {
						$contact->setFirstName($nameParts[0]);
						$contact->setMiddleName($nameParts[1]);
						$contact->setLastName($nameParts[2]);
					} else {
						$contact->setFirstName($nameParts[0]);
						$middleName = '';
						for ($i = 1; $i < count($nameParts)-1; ++$i) {
							$middleName .= $nameParts[i];
						}
						$contact->setMiddleName($middleName);
						$contact->setLastName($nameParts[count($nameParts)-1]);
					}
					$contact->setDisplayName($contactItem[2]);
					$contact->setContactEmail($contactItem[3]);
					$contact->setContactPhone($contactItem[4]);
					$em->persist($contact);
					$em->flush();
					$output->writeln('Created contact ' . $contact->getDisplayName() .  ' with id ' . $contact->getId());
				} else {
					$output->writeln($contact->getDisplayName() . ' already exists');
				}
			}

			// Go through again to make the role association
			$roles = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Role');
			$departments = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Department');
			$colleges = $this->getContainer()->get('doctrine')->getRepository('AppBundle:College');

			$llRole = $roles->findOneByRoleName('Liaison Librarian');
			if (!$llRole) {
				$output->writeln('Liaison Librarian role not found');
			} else {
				$currentContactName = "";
				$contact = null;
				$mapping = null;
				foreach($contacts as $contactItem) {
					if (! ($currentContactName == $contactItem[2])) {
						if ($mapping) {
							$em->persist($mapping);
							$em->flush();
						}
						$contact = $repository->findOneByDisplayName($contactItem[2]);
						$output->writeln('Creating new mappings for contact ' . $contact->getDisplayName());
						$currentContactName = $contact->getDisplayName();
						$mapping = new ContactRoleMapping();
						$mapping->setRole($llRole);
						$mapping->addContact($contact);
					}
					$department = $departments->findOneByName($contactItem[1]);
					if (!$department) {
						$output->writeln('Department ' . $contactItem[1] . ' not found');
					} else {
						$mapping->addDepartment($department);
					}
				}
				if ($mapping) {
					$em->persist($mapping);
					$em->flush();
				}
			}
		} else {
			$output->writeln('Could not open file ' . $inputPath . ', check to ensure it exists and is readable.');
		}		
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$inputType = $input->getArgument('type');

		if ($inputType === 'RGS') {
			$this->importRGS($input, $output);
		} elseif ($inputType === 'OGC') {
			$this->importOGC($input,$output);
		} elseif ($inputType === 'ES') {
			$this->importES($input, $output);
		} elseif ($inputType === 'LL') {
			$this->importLL($input, $output);
		} else {
			$output->writeln('Unknown input type ' . $inputType);
		}

	}
}