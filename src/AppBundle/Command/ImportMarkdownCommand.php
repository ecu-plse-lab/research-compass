<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Entity\StaticItem;

class ImportMarkdownCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('compass:import:markdown')
			->setDescription('Import Markdown descriptions into Research Compass')
			->addArgument(
				'path',
				InputArgument::REQUIRED,
				'Where is the base path from which to load?'
			);
	}

	private function loadSection($sectionNumber, $sectionName, $basePath, $em, $stepRepo, $stepSectionRepo)
	{
		$sectionPath = $basePath . "/{$sectionNumber}_{$sectionName}";
		$stepSection = $stepSectionRepo->findOneByName($sectionName);
		for ($i = 1; $i <= 12; ++$i) {
			$step = $stepRepo->findOneByOrdering($i);
			$filePath = $sectionPath . "/{$sectionName}-Step{$i}.md";
			if ($stepSection && $step && file_exists($filePath)) {
				$contents = file_get_contents($filePath);
				$item = new StaticItem();
				$item->setName('Step ' . $i . ', Section ' . $sectionName);
				$item->setContents($contents);
				$item->setStep($step);
				$item->setStepSection($stepSection);
				$em->persist($item);
				$em->flush();
			}

			$filePath = $sectionPath . "/{$sectionName}-Step{$i}-Master.md";
			if (file_exists($filePath)) {
				$contents = file_get_contents($filePath);
				$item = new StaticItem();
				$item->setName('Step ' . $i . ', Section ' . $sectionName . ', Master');
				$item->setContents($contents);
				$item->setStep($step);
				$item->setStepSection($stepSection);
				$em->persist($item);
				$em->flush();
			}

			$filePath = $sectionPath . "/{$sectionName}-Step{$i}-PersistentData.md";
			if (file_exists($filePath)) {
				$contents = file_get_contents($filePath);				
				$item = new StaticItem();
				$item->setName('Step ' . $i . ', Section ' . $sectionName . ', Persistent Data');
				$item->setContents($contents);
				$item->setStep($step);
				$item->setStepSection($stepSection);
				$em->persist($item);
				$em->flush();
			}
		}

	}
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$inputPath = $input->getArgument('path');
		$em = $this->getContainer()->get('doctrine')->getManager();
		$stepRepo = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Step');
		$stepSectionRepo = $this->getContainer()->get('doctrine')->getRepository('AppBundle:StepSection');

		// The files are organized by section, so process each section in turn
		// TODO: It may be good to softcode this, using the info on step sections in the database
		$output->writeln('Loading Section 1, Goals');
		$this->loadSection("1","Goals",$inputPath,$em,$stepRepo,$stepSectionRepo);

		$output->writeln('Loading Section 2, Actions');
		$this->loadSection("2","Actions",$inputPath,$em,$stepRepo,$stepSectionRepo);

		$output->writeln('Loading Section 3, Connections');
		$this->loadSection("3","Connections",$inputPath,$em,$stepRepo,$stepSectionRepo);

		$output->writeln('Loading Section 4, Resources');
		$this->loadSection("4","Resources",$inputPath,$em,$stepRepo,$stepSectionRepo);

		$output->writeln('Loading Section 5, Tools');
		$this->loadSection("5","Tools",$inputPath,$em,$stepRepo,$stepSectionRepo);

		$output->writeln('Loading Section 6, Outcomes');
		$this->loadSection("6","Outcomes",$inputPath,$em,$stepRepo,$stepSectionRepo);
	}
}