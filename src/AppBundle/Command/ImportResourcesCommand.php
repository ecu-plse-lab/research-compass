<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Entity\College;
use AppBundle\Entity\Department;
use AppBundle\Entity\LibraryResource;
use AppBundle\Entity\LibraryResourceCollege;
use AppBundle\Entity\LibraryResourceDepartment;

class ImportResourcesCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('compass:import:resources')
			->setDescription('Import information on library resources into Research Compass')
			->addArgument(
				'path',
				InputArgument::REQUIRED,
				'Where is the csv file to load?'
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$inputPath = $input->getArgument('path');
		$em = $this->getContainer()->get('doctrine')->getManager();
		$resourcesRepo = $this->getContainer()->get('doctrine')->getRepository('AppBundle:LibraryResource');
		$collegeRepo = $this->getContainer()->get('doctrine')->getRepository('AppBundle:College');
		$departmentRepo = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Department');

		if ( ($handle = fopen($inputPath, 'r')) !== FALSE ) {
			$output->writeln('Loading library resource information from file ' . $inputPath);
			while ($data = fgetcsv($handle, 0)) {
				$resourceInfo[] = $data;
			}
			fclose($handle);

			$output->writeln('Found ' . count($resourceInfo) . ' library resource info records');

			foreach($resourceInfo as $resourceRec) {
				$id = trim($resourceRec[0]);
				$title = trim($resourceRec[1]);
				$description = trim($resourceRec[2]);
				$libraryURL = trim($resourceRec[3]);
				$directURL = trim($resourceRec[4]);
				$format = trim($resourceRec[5]);
				$tag = trim($resourceRec[6]);
				$subjectsAsString = trim($resourceRec[7]);
				$collegesAsString = trim($resourceRec[8]);
				$departmentsAsString = trim($resourceRec[9]);

				// Subjects, colleges, and departments are lists, break these down into arrays
				$colleges = array_map('trim', explode(';', $collegesAsString));
				$departments = array_map('trim', explode(';', $departmentsAsString));
				$subjects = array_map('trim', explode(',', $subjectsAsString));

				// Create the resource entity
				$resource = new LibraryResource();
				$resource->setGivenId($id);
				$resource->setTitle($title);
				$resource->setDescription($description);
				$resource->setLibraryLink($libraryURL);
				$resource->setDirectLink($directURL);
				$resource->setFormat($format);
				$resource->setTag($tag);
				$resource->setSubjects($subjects);

				$em->persist($resource);

				// Add individual college and department records
				foreach ($colleges as $collegeName) {
					if (strlen($collegeName) > 0) {
						$college = $collegeRepo->findOneByName($collegeName);
						if (!$college) {
							$output->writeln('Invalid college given:' . $collegeName);
						}			

						$resourceCollege = new LibraryResourceCollege();
						$resourceCollege->setResource($resource);
						$resourceCollege->setCollege($college);
						$resource->addCollege($resourceCollege);
						$em->persist($resourceCollege);			
					}
				}

				foreach ($departments as $departmentName) {
					if (strlen($departmentName) > 0) {
						$department = $departmentRepo->findOneByName($departmentName);
						if (!$department) {
							$output->writeln('Invalid department given:' . $departmentName);
						}

						$resourceDepartment = new LibraryResourceDepartment();
						$resourceDepartment->setResource($resource);
						$resourceDepartment->setDepartment($department);
						$resource->addDepartment($resourceDepartment);
						$em->persist($resourceDepartment);
					}
				}

				$em->persist($resource);
				$em->flush();

				$output->writeln('Added resource ' . $resource->getTitle());
			}
		} else {
			$output->writeln('Could not open file ' . $inputPath . ', check to ensure it exists and is readable.');
		}

	}
}