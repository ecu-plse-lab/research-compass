<?php
namespace AppBundle\Security;

use AppBundle\Entity\User;
use KULeuven\ShibbolethBundle\Security\ShibbolethUserProviderInterface;
use KULeuven\ShibbolethBundle\Security\ShibbolethUserToken;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class CompassUserProvider implements ShibbolethUserProviderInterface
{
    private $doctrine;

    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    private function getIdFromEppn($eppn)
    {
	$userNameParts = explode("@", $eppn, 2);
	$userId = isset($userNameParts[0]) ? $userNameParts[0] : $eppn;
	return $userId;
    }

    public function loadUserByUsername($username)
    {
        $em = $this->doctrine->getManager();
	$username = $this->getIdFromEppn($username);
        $users = $em->getRepository('AppBundle:User')->findByUsername($username);
        if (count($users) == 1) {
            return $users[0];
        } else {
            throw new UsernameNotFoundException("User " . $username . " not found.");
        }
    }

    public function createUser(ShibbolethUserToken $token)
    {
        $em = $this->doctrine->getManager();

        // Create user object using shibboleth attributes stored in the token. 
        $user = new User();
	$username = $this->getIdFromEppn($token->getUsername());
	$user->setUsername($username);
	$user->setFirstName($token->getGivenName());
	$user->setLastName($token->getSurname());
	$displayName = trim($token->getGivenName());
	if (strlen($displayName) == 0) {
	    $displayName = trim($token->getSurname());
	} else {
	    $displayName .= ' ' . trim($token->getSurname());
        }
	$user->setDisplayName($displayName);
        $user->setEmail($token->getMail());

	$group = $em->getRepository('AppBundle:Group')->findOneByName('User');
	$user->addGroup($group);

	$em->persist($user);
	$em->flush();

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'AppBundle\Entity\User';
    }
}
