<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Layout
 *
 * @ORM\Table(name="layout")
 * @ORM\Entity
 */
class Layout
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @ORM\ManyToOne(targetEntity="Step", inversedBy="layouts")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id")
     */
	protected $step;

    /**
     * @ORM\ManyToOne(targetEntity="StepSection", inversedBy="layouts")
     * @ORM\JoinColumn(name="step_section_id", referencedColumnName="id")
     */
	protected $stepSection;

    /**
     * @ORM\OneToMany(targetEntity="LayoutItem", mappedBy="layout")
     */
    private $layoutItems;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
        $this->layoutItems = new ArrayCollection();
    }
	

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Layout
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Layout
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {

        return $this->modified;
    }

    /**
     * Set step
     *
     * @param \AppBundle\Entity\Step $step
     * @return Layout
     */
    public function setStep(\AppBundle\Entity\Step $step = null)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get step
     *
     * @return \AppBundle\Entity\Step 
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set stepSection
     *
     * @param \AppBundle\Entity\StepSection $stepSection
     * @return Layout
     */
    public function setStepSection(\AppBundle\Entity\StepSection $stepSection = null)
    {
        $this->stepSection = $stepSection;

        return $this;
    }

    /**
     * Get stepSection
     *
     * @return \AppBundle\Entity\StepSection 
     */
    public function getStepSection()
    {
        return $this->stepSection;
    }

    /**
     * Add layoutItems
     *
     * @param \AppBundle\Entity\LayoutItem $layoutItems
     * @return Layout
     */
    public function addLayoutItem(\AppBundle\Entity\LayoutItem $layoutItems)
    {
        $this->layoutItems[] = $layoutItems;

        return $this;
    }

    /**
     * Remove layoutItems
     *
     * @param \AppBundle\Entity\LayoutItem $layoutItems
     */
    public function removeLayoutItem(\AppBundle\Entity\LayoutItem $layoutItems)
    {
        $this->layoutItems->removeElement($layoutItems);
    }

    /**
     * Get layoutItems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLayoutItems()
    {
        return $this->layoutItems;
    }
}
