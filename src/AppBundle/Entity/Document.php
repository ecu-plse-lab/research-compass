<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Utils\Status;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Document
 *
 * @ORM\Table(name="document")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Document 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

     /**
      * @var string
      *
      * @ORM\Column(name="computed_name", type="string", length=100, nullable=false)
      */
    private $computedName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @ORM\OneToMany(targetEntity="DocumentLayoutItem", mappedBy="document")
     */
    private $layoutItems;

   /**
     * @Assert\File(maxSize="10000000")
     */
    private $file;

    public function __construct()
    {
        $this->status = Status::ACTIVE;
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
        $this->layoutItems = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Document
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set computed name
     *
     * @param string $name
     * @return ProjectDocument
     */
    public function setComputedName($name)
    {
        $this->computedName = $name;

        return $this;
    }

    /**
     * Get computed name
     *
     * @return string 
     */
    public function getComputedName()
    {
        return $this->computedName;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Document
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Document
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Document
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {

        return $this->modified;
    }

    /**
     * Add layoutItems
     *
     * @param \AppBundle\Entity\DocumentLayoutItem $layoutItems
     * @return Document
     */
    public function addLayoutItem(\AppBundle\Entity\DocumentLayoutItem $layoutItems)
    {
        $this->layoutItems[] = $layoutItems;

        return $this;
    }

    /**
     * Remove layoutItems
     *
     * @param \AppBundle\Entity\DocumentLayoutItem $layoutItems
     */
    public function removeLayoutItem(\AppBundle\Entity\DocumentLayoutItem $layoutItems)
    {
        $this->layoutItems->removeElement($layoutItems);
    }

    /**
     * Get layoutItems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLayoutItems()
    {
        return $this->layoutItems;
    }

    public function getAbsolutePath()
    {
        return null === $this->computedName
            ? null
            : $this->getUploadRootDir().'/'.$this->computedName;
    }

    public function getWebPath()
    {
        return null === $this->computedName
            ? null
            : $this->getUploadDir().'/'.$this->computedName;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/compass/documents';
    }    

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        // If we already have a file associated with this entity, keep track
        // of the current computed name so we can remove it later.
        if (isset($this->computedName)) {
            $this->oldfile = $this->computedName;
        }

        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            $file = $this->getFile();

            // Set the original filename so we can still show it to the user
            $this->setName($file->getClientOriginalName());

            // Generate a unique name for the file, based on the current extension
            // and a random base name.
            $extension = $file->guessExtension();
            if (!$extension) {
                $extension = 'bin';
            }
            $this->setComputedName(sha1(uniqid(mt_rand(), true)) . '.' . $extension);
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->getComputedName());

        // check if we have an old file to remove
        if (isset($this->oldFile)) {
            // delete the old file
            unlink($this->getUploadRootDir().'/'.$this->oldFile);
            // clear the temp image path
            $this->oldFile = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }
}
