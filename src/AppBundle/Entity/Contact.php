<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Utils\Status;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;

/**
 * Contact
 *
 * @ORM\Table(name="contact")
 * @ORM\Entity
 * @UniqueEntity("contactEmail")
 */

class Contact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=60, nullable=false)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=60, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="middle_name", type="string", length=60, nullable=true)
     */
    private $middleName;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=60, nullable=true)
     *
     */
    private $displayName;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="contact_email", type="string", length=60, nullable=false, unique=true)
     *
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */

    protected $contactEmail;

    /**
     * @var string
     *
     * @AssertPhoneNumber(defaultRegion="US")
     *
     * @ORM\Column(name="contact_phone", type="string", length=12, nullable=false)
     */

    private $contactPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="office_location", type="string", length=60, nullable=true)
     */
    private $officeLocation;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @ORM\ManyToMany(targetEntity="ContactRoleMapping", mappedBy="contacts")
     * @ORM\JoinTable(name="contact_role_xref")
     */
    private $mappings;

    /**
     * @ORM\OneToMany(targetEntity="ContactLayoutItem", mappedBy="contact")
     */
    private $layoutItems;

    public function __construct()
    {
        $this->mappings = new ArrayCollection();
        $this->status = Status::ACTIVE;
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
        $this->layoutItems = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Contact
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Contact
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     * @return Contact
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName
     *
     * @return string 
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     * @return Contact
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string 
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set contactEmail
     *
     * @param string $contactEmail
     * @return Contact
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * Get contactEmail
     *
     * @return string 
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * Set contactPhone
     *
     * @param string $contactPhone
     * @return Contact
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    /**
     * Get contactPhone
     *
     * @return string 
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * Set officeLocation
     *
     * @param string $officeLocation
     * @return Contact
     */
    public function setOfficeLocation($officeLocation)
    {
        $this->officeLocation = $officeLocation;

        return $this;
    }

    /**
     * Get officeLocation
     *
     * @return string 
     */
    public function getOfficeLocation()
    {
        return $this->officeLocation;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Contact
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Contact
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Contact
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {

        return $this->modified;
    }

    /**
     * Add roles
     *
     * @param \AppBundle\Entity\Role $roles
     * @return Contact
     */
    public function addRole(\AppBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \AppBundle\Entity\Role $roles
     */
    public function removeRole(\AppBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Add layoutItems
     *
     * @param \AppBundle\Entity\ContactLayoutItem $layoutItems
     * @return Contact
     */
    public function addLayoutItem(\AppBundle\Entity\ContactLayoutItem $layoutItems)
    {
        $this->layoutItems[] = $layoutItems;

        return $this;
    }

    /**
     * Remove layoutItems
     *
     * @param \AppBundle\Entity\ContactLayoutItem $layoutItems
     */
    public function removeLayoutItem(\AppBundle\Entity\ContactLayoutItem $layoutItems)
    {
        $this->layoutItems->removeElement($layoutItems);
    }

    /**
     * Get layoutItems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLayoutItems()
    {
        return $this->layoutItems;
    }

    public function __toString()
    {
        return $this->getDisplayName();
    }
}
