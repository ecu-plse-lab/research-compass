<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="item_type", type="string")
 * @ORM\DiscriminatorMap({
 *   "link" = "LinkLayoutItem", 
 *   "document" = "DocumentLayoutItem", 
 *   "contact" = "ContactLayoutItem", 
 *   "role" = "RoleLayoutItem", 
 *   "static" = "StaticLayoutItem",
 *   "resource" = "LibraryResourceLayoutItem"})
 * @ORM\Table(name="layout_item") 
 */
abstract class LayoutItem 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

	/**
	 * @var integer
     *
     * @ORM\Column(name="item_order", type="integer", nullable=false)
     *
	 */
	protected $order;

    /**
     * @ORM\ManyToOne(targetEntity="Layout", inversedBy="layoutItems")
     * @ORM\JoinColumn(name="layout_id", referencedColumnName="id")
     */
    protected $layout;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return LayoutItem
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set layout
     *
     * @param \AppBundle\Entity\Layout $layout
     * @return LayoutItem
     */
    public function setLayout(\AppBundle\Entity\Layout $layout = null)
    {
        $this->layout = $layout;

        return $this;
    }

    /**
     * Get layout
     *
     * @return \AppBundle\Entity\Layout 
     */
    public function getLayout()
    {
        return $this->layout;
    }
}
