<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Utils\Status;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Contact
 *
 * @ORM\Table(name="role")
 * @ORM\Entity
 * @UniqueEntity("roleName")
 */
class Role
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="role_name", type="string", length=60, nullable=false)
     */
    private $roleName;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @ORM\OneToMany(targetEntity="ContactRoleMapping", mappedBy="role")
     */
    private $mappings;

    /**
     * @ORM\OneToMany(targetEntity="RoleLayoutItem", mappedBy="role")
     */
    private $layoutItems;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->status = Status::ACTIVE;
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
        $this->layoutItems = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set roleName
     *
     * @param string $roleName
     * @return Role
     */
    public function setRoleName($roleName)
    {
        $this->roleName = $roleName;

        return $this;
    }

    /**
     * Get roleName
     *
     * @return string 
     */
    public function getRoleName()
    {
        return $this->roleName;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Role
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Role
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Role
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {

        return $this->modified;
    }

    /**
     * Add contacts
     *
     * @param \AppBundle\Entity\Contact $contacts
     * @return Role
     */
    public function addContact(\AppBundle\Entity\Contact $contacts)
    {
        $this->contacts[] = $contacts;

        return $this;
    }

    /**
     * Remove contacts
     *
     * @param \AppBundle\Entity\Contact $contacts
     */
    public function removeContact(\AppBundle\Entity\Contact $contacts)
    {
        $this->contacts->removeElement($contacts);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Add layoutItems
     *
     * @param \AppBundle\Entity\RoleLayoutItem $layoutItems
     * @return Role
     */
    public function addLayoutItem(\AppBundle\Entity\RoleLayoutItem $layoutItems)
    {
        $this->layoutItems[] = $layoutItems;

        return $this;
    }

    /**
     * Remove layoutItems
     *
     * @param \AppBundle\Entity\RoleLayoutItem $layoutItems
     */
    public function removeLayoutItem(\AppBundle\Entity\RoleLayoutItem $layoutItems)
    {
        $this->layoutItems->removeElement($layoutItems);
    }

    /**
     * Get layoutItems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLayoutItems()
    {
        return $this->layoutItems;
    }

    /**
     * Add contact x role mapping
     *
     * @param \AppBundle\Entity\ContactRoleMapping $mapping
     * @return Role
     */
    public function addMapping(\AppBundle\Entity\ContactRoleMapping $mapping)
    {
        $this->mappings[] = $mapping;

        return $this;
    }

    /**
     * Remove contact x role mapping
     *
     * @param \AppBundle\Entity\ContactRoleMapping $mapping
     */
    public function removeMapping(\AppBundle\Entity\ContactRoleMapping $mapping)
    {
        $this->mappings->removeElement($mapping);
    }

    /**
     * Get contact x role mappings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMappings()
    {
        return $this->mappings;
    }

    public function __toString()
    {
        return $this->getRoleName();
    }

    /**
     * Get the contacts for this role, given the department and existing mappings.
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function contactsForRole(\AppBundle\Entity\Department $department)
    {
        foreach ($this->getMappings() as $mapping) {
            if ($mapping->containsDepartment($department)) {
                return $mapping->getContacts();
            }
        }

        foreach ($this->getMappings() as $mapping) {
            if ($mapping->containsCollege($department->getCollege())) {
                return $mapping->getContacts();
            }
        }
    }
}
