<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="contact_role_mapping") 
 */
class ContactRoleMapping 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Contact", inversedBy="mappings")
     * @ORM\JoinTable(name="contact_role_xref")
     */
    private $contacts;

    /**
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="mappings")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    private $role;

    /**
     * @ORM\ManyToMany(targetEntity="College")
     * @ORM\JoinTable(name="contact_role_college_xref", 
     *   joinColumns={@ORM\JoinColumn(name="mapping_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="college_id", referencedColumnName="id")}
     *   )
     */
    private $colleges;

    /**
     * @ORM\ManyToMany(targetEntity="Department")
     * @ORM\JoinTable(name="contact_role_department_xref", 
     *   joinColumns={@ORM\JoinColumn(name="mapping_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="department_id", referencedColumnName="id")}
     *   )
     */
    private $departments;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    public function __construct()
    {
        $this->colleges = new ArrayCollection();
        $this->departments = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ContactRoleMapping
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ContactRoleMapping
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Add contacts
     *
     * @param \AppBundle\Entity\Contact $contacts
     * @return ContactRoleMapping
     */
    public function addContact(\AppBundle\Entity\Contact $contacts)
    {
        $this->contacts[] = $contacts;

        return $this;
    }

    /**
     * Remove contacts
     *
     * @param \AppBundle\Entity\Contact $contacts
     */
    public function removeContact(\AppBundle\Entity\Contact $contacts)
    {
        $this->contacts->removeElement($contacts);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Set role
     *
     * @param \AppBundle\Entity\Role $role
     * @return ContactRoleMapping
     */
    public function setRole(\AppBundle\Entity\Role $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \AppBundle\Entity\Role 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Add colleges
     *
     * @param \AppBundle\Entity\College $colleges
     * @return ContactRoleMapping
     */
    public function addCollege(\AppBundle\Entity\College $colleges)
    {
        $this->colleges[] = $colleges;

        return $this;
    }

    /**
     * Remove colleges
     *
     * @param \AppBundle\Entity\College $colleges
     */
    public function removeCollege(\AppBundle\Entity\College $colleges)
    {
        $this->colleges->removeElement($colleges);
    }

    /**
     * Get colleges
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getColleges()
    {
        return $this->colleges;
    }

    /**
     * Add departments
     *
     * @param \AppBundle\Entity\Department $departments
     * @return ContactRoleMapping
     */
    public function addDepartment(\AppBundle\Entity\Department $departments)
    {
        $this->departments[] = $departments;

        return $this;
    }

    /**
     * Remove departments
     *
     * @param \AppBundle\Entity\Department $departments
     */
    public function removeDepartment(\AppBundle\Entity\Department $departments)
    {
        $this->departments->removeElement($departments);
    }

    /**
     * Get departments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDepartments()
    {
        return $this->departments;
    }

    /**
     * Check to see if the given department is contained in this mapping.
     *
     * @param \AppBundle\Entity\Department $departments
     *
     * @return boolean
     */
    public function containsDepartment(\AppBundle\Entity\Department $department)
    {
        return $this->departments->contains($department);
    }

    /**
     * Check to see if the given college is contained in this mapping.
     *
     * @param \AppBundle\Entity\College $college
     *
     * @return boolean
     */
    public function containsCollege(\AppBundle\Entity\College $college)
    {
        return $this->colleges->contains($college);
    }

}
