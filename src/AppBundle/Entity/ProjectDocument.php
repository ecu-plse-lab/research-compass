<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Utils\Status;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * ProjectDocument
 *
 * @ORM\Table(name="project_document")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ProjectDocument
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="documents")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false)
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity="Step")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id", nullable=false)
     */
    private $step;

    /**
     * @ORM\ManyToOne(targetEntity="StepSection")
     * @ORM\JoinColumn(name="step_section_id", referencedColumnName="id", nullable=false)
     */
    private $section;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

     /**
      * @var string
      *
      * @ORM\Column(name="computed_name", type="string", length=100, nullable=false)
      */
    private $computedName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

   /**
     * @Assert\File(maxSize="10000000")
     */
    private $file;

    private $oldFile;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProjectDocument
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set computed name
     *
     * @param string $name
     * @return ProjectDocument
     */
    public function setComputedName($name)
    {
        $this->computedName = $name;

        return $this;
    }

    /**
     * Get computed name
     *
     * @return string 
     */
    public function getComputedName()
    {
        return $this->computedName;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProjectDocument
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProjectDocument
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project $project
     * @return ProjectDocument
     */
    public function setProject(\AppBundle\Entity\Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set step
     *
     * @param \AppBundle\Entity\Step $step
     * @return ProjectDocument
     */
    public function setStep(\AppBundle\Entity\Step $step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get step
     *
     * @return \AppBundle\Entity\Step 
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set section
     *
     * @param \AppBundle\Entity\StepSection $section
     * @return ProjectDocument
     */
    public function setSection(\AppBundle\Entity\StepSection $section)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return \AppBundle\Entity\StepSection 
     */
    public function getSection()
    {
        return $this->section;
    }

    public function getAbsolutePath()
    {
        return null === $this->computedName
            ? null
            : $this->getUploadRootDir().'/'.$this->computedName;
    }

    public function getWebPath()
    {
        return null === $this->computedName
            ? null
            : $this->getUploadDir().'/'.$this->computedName;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/project/' . $this->project->getId() . '/documents';
    }    

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        // If we already have a file associated with this entity, keep track
        // of the current computed name so we can remove it later.
        if (isset($this->computedName)) {
            $this->oldfile = $this->computedName;
        }

        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            $file = $this->getFile();

            // Set the original filename so we can still show it to the user
            $this->setName($file->getClientOriginalName());

            // Generate a unique name for the file, based on the current extension
            // and a random base name.
            $extension = $file->guessExtension();
            if (!$extension) {
                $extension = 'bin';
            }
            $this->setComputedName(sha1(uniqid(mt_rand(), true)) . '.' . $extension);
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->getComputedName());

        // check if we have an old file to remove
        if (isset($this->oldFile)) {
            // delete the old file
            unlink($this->getUploadRootDir().'/'.$this->oldFile);
            // clear the temp image path
            $this->oldFile = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }
}
