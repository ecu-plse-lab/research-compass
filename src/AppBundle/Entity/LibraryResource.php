<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Utils\Status;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * LibraryResource
 *
 * @ORM\Table(name="library_resource")
 * @ORM\Entity
 */
class LibraryResource
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="givenId", type="integer")
     */
    private $givenId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="libraryLink", type="string", length=255, nullable=false)
     */
    private $libraryLink;

    /**
     * @var string
     *
     * @ORM\Column(name="directLink", type="string", length=255, nullable=false)
     */
    private $directLink;

    /**
     * @var string
     *
     * @ORM\Column(name="format", type="string", length=100, nullable=true)
     */
    private $format;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=100, nullable=false)
     */
    private $tag;

    /**
     * @ORM\Column(name="subjects", type="array", nullable=false)
     */
    private $subjects;

    /**
     * @ORM\OneToMany(targetEntity="LibraryResourceCollege", mappedBy="resource")
     */
    private $colleges;

    /**
     * @ORM\OneToMany(targetEntity="LibraryResourceDepartment", mappedBy="resource")
     */
    private $departments;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    public function __construct()
    {
        $this->status = Status::ACTIVE;
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set givenId
     *
     * @param integer $givenId
     * @return LibraryResource
     */
    public function setGivenId($givenId)
    {
        $this->givenId = $givenId;

        return $this;
    }

    /**
     * Get givenId
     *
     * @return integer 
     */
    public function getGivenId()
    {
        return $this->givenId;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return LibraryResource
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return LibraryResource
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set libraryLink
     *
     * @param string $libraryLink
     * @return LibraryResource
     */
    public function setLibraryLink($libraryLink)
    {
        $this->libraryLink = $libraryLink;

        return $this;
    }

    /**
     * Get libraryLink
     *
     * @return string 
     */
    public function getLibraryLink()
    {
        return $this->libraryLink;
    }

    /**
     * Set directLink
     *
     * @param string $directLink
     * @return LibraryResource
     */
    public function setDirectLink($directLink)
    {
        $this->directLink = $directLink;

        return $this;
    }

    /**
     * Get directLink
     *
     * @return string 
     */
    public function getDirectLink()
    {
        return $this->directLink;
    }

    /**
     * Set format
     *
     * @param string $format
     * @return LibraryResource
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return string 
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return LibraryResource
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set subjects
     *
     * @param array $subjects
     * @return LibraryResource
     */
    public function setSubjects($subjects)
    {
        $this->subjects = $subjects;

        return $this;
    }

    /**
     * Get subjects
     *
     * @return array 
     */
    public function getSubjects()
    {
        return $this->subjects;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return LibraryResource
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return LibraryResource
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return LibraryResource
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Add colleges
     *
     * @param \AppBundle\Entity\LibraryResourceCollege $colleges
     * @return LibraryResource
     */
    public function addCollege(\AppBundle\Entity\LibraryResourceCollege $colleges)
    {
        $this->colleges[] = $colleges;

        return $this;
    }

    /**
     * Remove colleges
     *
     * @param \AppBundle\Entity\LibraryResourceCollege $colleges
     */
    public function removeCollege(\AppBundle\Entity\LibraryResourceCollege $colleges)
    {
        $this->colleges->removeElement($colleges);
    }

    /**
     * Get colleges
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getColleges()
    {
        return $this->colleges;
    }

    /**
     * Add departments
     *
     * @param \AppBundle\Entity\LibraryResourceDepartment $departments
     * @return LibraryResource
     */
    public function addDepartment(\AppBundle\Entity\LibraryResourceDepartment $departments)
    {
        $this->departments[] = $departments;

        return $this;
    }

    /**
     * Remove departments
     *
     * @param \AppBundle\Entity\LibraryResourceDepartment $departments
     */
    public function removeDepartment(\AppBundle\Entity\LibraryResourceDepartment $departments)
    {
        $this->departments->removeElement($departments);
    }

    /**
     * Get departments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDepartments()
    {
        return $this->departments;
    }

}
