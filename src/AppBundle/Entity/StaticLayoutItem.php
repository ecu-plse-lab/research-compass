<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class StaticLayoutItem extends LayoutItem {
    /**
     * @ORM\ManyToOne(targetEntity="StaticItem", inversedBy="layoutItems")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
	protected $staticItem;

    /**
     * Set staticItem
     *
     * @param \AppBundle\Entity\StaticItem $staticItem
     * @return StaticLayoutItem
     */
    public function setStaticItem(\AppBundle\Entity\StaticItem $staticItem = null)
    {
        $this->staticItem = $staticItem;

        return $this;
    }

    /**
     * Get staticItem
     *
     * @return \AppBundle\Entity\StaticItem 
     */
    public function getStaticItem()
    {
        return $this->staticItem;
    }
}
