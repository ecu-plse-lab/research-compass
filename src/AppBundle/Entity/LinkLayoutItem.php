<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class LinkLayoutItem extends LayoutItem 
{
    /**
     * @ORM\ManyToOne(targetEntity="Link", inversedBy="layoutItems")
     * @ORM\JoinColumn(name="link_id", referencedColumnName="id")
     */
	protected $link;

    /**
     * Set link
     *
     * @param \AppBundle\Entity\Link $link
     * @return LinkLayoutItem
     */
    public function setLink(\AppBundle\Entity\Link $link = null)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return \AppBundle\Entity\Link 
     */
    public function getLink()
    {
        return $this->link;
    }
}
