<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class RoleLayoutItem extends LayoutItem 
{
    /**
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="layoutItems")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
	protected $role;

    /**
     * Set role
     *
     * @param \AppBundle\Entity\Role $role
     * @return RoleLayoutItem
     */
    public function setRole(\AppBundle\Entity\Role $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \AppBundle\Entity\Role 
     */
    public function getRole()
    {
        return $this->role;
    }
}
