<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Utils\Status;

/**
 * StepSection
 *
 * @ORM\Table(name="step_section")
 * @ORM\Entity
 */
class StepSection
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var integer
     * @ORM\Column(name="step_section_order", type="integer", nullable=false)
     */
    private $ordering;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @ORM\OneToMany(targetEntity="StaticItem", mappedBy="stepSection")
     */
    private $staticItems;

    /**
     * @ORM\OneToMany(targetEntity="Layout", mappedBy="stepSection")
     */
    private $layouts;

    public function __construct()
    {
        $this->staticItems = new ArrayCollection();
        $this->status = Status::ACTIVE;
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
        $this->layouts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return StepSection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return StepSection
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get ordering
     *
     * @return integer 
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * Set ordering
     *
     * @param integer $ordering
     * @return StepSection
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return StepSection
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return StepSection
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Add staticItems
     *
     * @param \AppBundle\Entity\StaticItem $staticItems
     * @return StepSection
     */
    public function addStaticItem(\AppBundle\Entity\StaticItem $staticItems)
    {
        $this->staticItems[] = $staticItems;

        return $this;
    }

    /**
     * Remove staticItems
     *
     * @param \AppBundle\Entity\StaticItem $staticItems
     */
    public function removeStaticItem(\AppBundle\Entity\StaticItem $staticItems)
    {
        $this->staticItems->removeElement($staticItems);
    }

    /**
     * Get staticItems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStaticItems()
    {
        return $this->staticItems;
    }

    /**
     * Add layouts
     *
     * @param \AppBundle\Entity\Layout $layouts
     * @return StepSection
     */
    public function addLayout(\AppBundle\Entity\Layout $layouts)
    {
        $this->layouts[] = $layouts;

        return $this;
    }

    /**
     * Remove layouts
     *
     * @param \AppBundle\Entity\Layout $layouts
     */
    public function removeLayout(\AppBundle\Entity\Layout $layouts)
    {
        $this->layouts->removeElement($layouts);
    }

    /**
     * Get layouts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLayouts()
    {
        return $this->layouts;
    }

    public function __toString()
    {
        return $this->getName();
    }
    
}
