<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class DocumentLayoutItem extends LayoutItem 
{
    /**
     * @ORM\ManyToOne(targetEntity="Document", inversedBy="layoutItems")
     * @ORM\JoinColumn(name="document_id", referencedColumnName="id")
     */
	protected $document;

    /**
     * Set document
     *
     * @param \AppBundle\Entity\Document $document
     * @return DocumentLayoutItem
     */
    public function setDocument(\AppBundle\Entity\Document $document = null)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return \AppBundle\Entity\Document 
     */
    public function getDocument()
    {
        return $this->document;
    }
}
