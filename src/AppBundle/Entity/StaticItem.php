<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Utils\Status;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * StaticItem
 *
 * @ORM\Table(name="static_item")
 * @ORM\Entity
 */
class StaticItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="contents", type="text", nullable=false)
     */
    private $contents;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @ORM\ManyToOne(targetEntity="Step", inversedBy="staticItems")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id")
     */
    private $step;

    /**
     * @ORM\ManyToOne(targetEntity="StepSection", inversedBy="staticItems")
     * @ORM\JoinColumn(name="step_section_id", referencedColumnName="id")
     */
    private $stepSection;

    /**
     * @ORM\OneToMany(targetEntity="StaticLayoutItem", mappedBy="staticItem")
     */
    private $layoutItems;

    public function __construct()
    {
        $this->status = Status::ACTIVE;
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
        $this->layoutItems = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return StaticItem
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set contents
     *
     * @param string $contents
     * @return StaticItem
     */
    public function setContents($contents)
    {
        $this->contents = $contents;

        return $this;
    }

    /**
     * Get contents
     *
     * @return string 
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return StaticItem
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return StaticItem
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return StaticItem
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set step
     *
     * @param \AppBundle\Entity\Step $step
     * @return StaticItem
     */
    public function setStep(\AppBundle\Entity\Step $step = null)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get step
     *
     * @return \AppBundle\Entity\Step 
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set stepSection
     *
     * @param \AppBundle\Entity\StepSection $stepSection
     * @return StaticItem
     */
    public function setStepSection(\AppBundle\Entity\StepSection $stepSection = null)
    {
        $this->stepSection = $stepSection;

        return $this;
    }

    /**
     * Get stepSection
     *
     * @return \AppBundle\Entity\StepSection 
     */
    public function getStepSection()
    {
        return $this->stepSection;
    }

    /**
     * Add layoutItems
     *
     * @param \AppBundle\Entity\StaticLayoutItem $layoutItems
     * @return StaticItem
     */
    public function addLayoutItem(\AppBundle\Entity\StaticLayoutItem $layoutItems)
    {
        $this->layoutItems[] = $layoutItems;

        return $this;
    }

    /**
     * Remove layoutItems
     *
     * @param \AppBundle\Entity\StaticLayoutItem $layoutItems
     */
    public function removeLayoutItem(\AppBundle\Entity\StaticLayoutItem $layoutItems)
    {
        $this->layoutItems->removeElement($layoutItems);
    }

    /**
     * Get layoutItems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLayoutItems()
    {
        return $this->layoutItems;
    }
}
