<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Utils\Status;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity
 */
class Project
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="projects")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $owner;

    /**
     * @var string
     *
     * @ORM\Column(name="project_name", type="string", length=70, nullable=false)
     */
    private $projectName;

    /**
     * @ORM\ManyToOne(targetEntity="Department")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id", nullable=false)
     */
    private $department;

    /**
     * @var string
     *
     * @ORM\Column(name="project_desc", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @ORM\ManyToOne(targetEntity="Step")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id", nullable=false)
     */
    private $step;

    /**
     * @ORM\ManyToOne(targetEntity="StepSection")
     * @ORM\JoinColumn(name="step_section_id", referencedColumnName="id", nullable=true)
     */
    private $section;

    /**
     * @ORM\OneToMany(targetEntity="SharedProject", mappedBy="project")
     */
    private $sharedProjects;

    /**
     * @ORM\Column(name="item_status", type="simple_array", nullable=false)
     */
    private $itemStatus;

    /**
     * @ORM\OneToMany(targetEntity="ProjectDocument", mappedBy="project")
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="ProjectNote", mappedBy="project")
     */
    private $notes;

    public function __construct()
    {
        $this->status = Status::ACTIVE;
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
        $this->sharedProjects = new ArrayCollection();
        $this->itemStatus = array();
        $this->documents = new ArrayCollection();
        $this->notes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user that owns this project
     *
     * @param AppBundle\Entity\User $user
     * @return Project
     */
    public function setOwner($user)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get user that owns this project
     *
     * @return AppBundle\Entity\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set projectName
     *
     * @param string $projectName
     * @return Project
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;

        return $this;
    }

    /**
     * Get projectName
     *
     * @return string 
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set department
     *
     * @param AppBundle\Entity\Department
     * @return Project
     */
    public function setDepartment($department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return AppBundle\Entity\Department 
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Project
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Project
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Project
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set current step
     *
     * @param Step $step
     * @return Project
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get current step
     *
     * @return Step
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set current section
     *
     * @param StepSection $section
     * @return Project
     */
    public function setSection($section)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get current section
     *
     * @return StepSection
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Add shared project
     *
     * @param \AppBundle\Entity\SharedProject $project
     * @return Project
     */
    public function addSharedProject(\AppBundle\Entity\SharedProject $project)
    {
        $this->sharedProjects[] = $project;

        return $this;
    }

    /**
     * Remove shared project
     *
     * @param \AppBundle\Entity\SharedProject $project
     */
    public function removeSharedProject(\AppBundle\Entity\SharedProject $project)
    {
        $this->sharedProjects->removeElement($project);
    }

    /**
     * Get shared projects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSharedProjects()
    {
        return $this->sharedProjects;
    }

    /**
     * Set the entire item status array
     *
     * @param array $itemStatus
     * @return Project
     */
    public function setItemStatus($itemStatus)
    {
        $this->itemStatus = $itemStatus;

        return $this;
    }

    /**
     * Get the entire item status array
     *
     * @return array
     */
    public function getItemStatus()
    {
        return $this->itemStatus;
    }

    /**
     * Set the status for a specific item
     * 
     * @param integer $id
     * @param boolean $val
     *
     * @return Project
     */
    public function setItemStatusForId($id, $val)
    {
        $this->itemStatus[$id] = $val;

        return $this;
    } 

    /**
     * Get the status for a specific item
     * 
     * @param integer $id
     *
     * @return boolean
     */
    public function getItemStatusForId($id) 
    {
        if (isset($this->itemStatus[$id])) {
            return $this->itemStatus[$id];
        }
        return false;
    }

    /**
     * Set the status for a specific item to true
     * 
     * @param integer $id
     *
     * @return Project
     */
    public function markItemComplete($id)
    {
        return $this->setItemStatusForId($id, true);
    }

    /**
     * Set the status for a specific item to false
     * 
     * @param integer $id
     *
     * @return Project
     */
    public function markItemIncomplete($id)
    {
        return $this->setItemStatusForId($id, false);
    }

    /**
     * Add project document
     *
     * @param \AppBundle\Entity\ProjectDocument $document
     * @return Project
     */
    public function addDocument(\AppBundle\Entity\ProjectDocument $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove project document
     *
     * @param \AppBundle\Entity\ProjectDocument $document
     */
    public function removeDocument(\AppBundle\Entity\ProjectDocument $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get project documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocuments()
    {
        return $this->documents;
    }    

    /**
     * Add project note
     *
     * @param \AppBundle\Entity\ProjectNote $note
     * @return Project
     */
    public function addNote(\AppBundle\Entity\ProjectNote $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove project note
     *
     * @param \AppBundle\Entity\ProjectNote $note
     */
    public function removeNote(\AppBundle\Entity\ProjectNote $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get project notes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotes()
    {
        return $this->notes;
    }        
}
