<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ContactLayoutItem extends LayoutItem 
{
    /**
     * @ORM\ManyToOne(targetEntity="Contact", inversedBy="layoutItems")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
	protected $contact;

    /**
     * Set contact
     *
     * @param \AppBundle\Entity\Contact $contact
     * @return ContactLayoutItem
     */
    public function setContact(\AppBundle\Entity\Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \AppBundle\Entity\Contact 
     */
    public function getContact()
    {
        return $this->contact;
    }
}
